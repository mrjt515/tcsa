<section id="home-blocks" class="w960 col3 clearfix">
	
	<article class="block item1">
		<a href="<? echo $page_url1; ?>" title="<? echo $page_title1; ?>"><img src="/assets/img/soccer-resources.jpg" width="300" height="135" alt="<? echo $page_title1; ?>" /></a>
		<h2><span><em>Free</em> Resources</span></h2>
		<p>Download a free ebook explaining our philosophy and what you can expect from The Champion’s Soccer Academy.</p>
		<p class="btn-container"><a href="<? echo $page_url1; ?>" class="btn btn-style1" title="<? echo $page_title1; ?>"><? echo $nav_menu1; ?></a></p>	
	</article>
	
	<article class="block item2">
		<a href="<? echo $page_url2; ?>" title="<? echo $page_title2; ?>"><img src="/assets/img/soccer-courses.jpg" width="300" height="135" alt="<? echo $page_title2; ?>" /></a>
		<h2><span><em>Our</em> Courses</span></h2>
		<p>Our coaching curriculum goes way beyond the mandatory standards expected of junior and youth coaches.</p>
		<p class="btn-container"><a href="<? echo $page_url2; ?>" class="btn" title="<? echo $page_title2; ?>">View our programmes</a></p>
	</article>
	
	<article class="block item3">
		<a href="<? echo $events_url; ?>" accesskey=""title="<? echo $events_linktitle; ?>"><img src="/assets/img/soccer-matchday.jpg" width="300" height="135" alt="<? echo $events_linktitle; ?>" /></a>
		<h2><span>Match Day Events</span></h2>
		<p>Catch up with the latest news and events about match days including team fixtures and trainign schedules.</p>
		<p class="btn-container"><a href="<? echo $events_url; ?>" class="btn" title="<? echo $events_linktitle; ?>">Read More</a></p>
	</article>

</section>