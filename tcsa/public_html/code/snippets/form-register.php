<form action="http://thechampionssocceracademy.createsend.com/t/j/s/jhzdy/" method="post" autocomplete="on" id="contactform" class="pageform clearfix">
	<h3 class="form-title">Register for your FREE eBook</h3>
	<fieldset><legend>Your Details</legend>
		<div>
		  <span class="label"><label for="name">Name:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. John Smith" name="cm-name" id="name" class="txtbox" /></span>
		</div>
		
		<div>
			<span class="label"><label for="jhzdy-jhzdy">Email:</label></span>
			<span class="data"><input type="email" placeholder="e.g. johnsmith@myemail.com" name="cm-jhzdy-jhzdy" id="jhzdy-jhzdy" class="txtbox emailadd required" required="required" /><br />
<small class="fieldinstruction">Email is required for us to send you the link to download the Free eBook</small></span>
		</div>
		
		<div>
		  <span class="spacer">&nbsp;</span>
		  <span class="data multiple"><input type="checkbox" name="cm-ol-jdvn" id="WebsiteNewsletterSignup" checked="checked" /> <label for="WebsiteNewsletterSignup">I would like to subscribe to the TCSA newsletter</label></span>
		</div>		
	</fieldset>
	
	<fieldset class="last">
		<div>
			<span class="spacer">&nbsp;</span>
			<span class="btn-container"><input type="submit" value="Download eBook" class="btn submitbtn" /></span>
		</div>
	</fieldset>	   
</form>