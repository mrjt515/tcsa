<section id="pageblock-tcsa-one" class="w960 boxgrid col3 clearfix">

	<article id="cb-staff" class="block item1">
		<h3 class="title t-staff">Meet The Coaches<br /><span>View members of our coaching staff</span></h3>		
		<ul class="coach-idtag clearfix">
			<li><img src="/assets/img/avatar-frankie-burrows.png" width="100" height="100" alt="<? echo $coachname_1; ?>" /></li>
			<li class="coach-label"><span class="staff-title"><? echo $coachtitle_1; ?></span><br /><span class="staff-name"><? echo $coachname_1; ?></span><br /><a href="https://twitter.com/<? echo $coachtwitter_1; ?>" class="twitter-follow-button" data-show-count="false">Follow @<? echo $coachtwitter_1; ?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></li>
		</ul>		
		<?php /*?><ul class="blocklist">
			<li><a href="/coaching-team.php" title="Meet The Coaches">Meet the TCSA Coaching team</a></li>
			<li><a href="/coaching-philosophy.php" title="Our Coaching Philosophy">Our coaching standards</a></li>
		</ul><?php */?>
	</article>
	
	<article id="cb-courses" class="block item2">
		<h3 class="title t-prog">Programmes<br /><span>Overview of our courses</span></h3>
		<ul class="linklist">
			<?php include("code/snippets/menu-services.php"); ?>
		</ul>
	</article>
	
	<article id="cb-blog" class="block item3 last">
		<h3 class="title t-blog">Our Blog<br /><span>Latest updates from TCSA</span></h3>
		<article class="blogpost">
			<a href="/blog.php#blogpost3" title="Read Blog Post" class="blogthumb"><img src="/assets/img/news/welshsupercup-thumb.jpg" width="220" height="135" alt="TCSA will be at the Welsh Super Cup" /><span class="bloglink-arrow"></span></a>
			<h6 class="datestamp">23/07/2012</h6>
			<h3 class="blog-headline"><a href="/blog.php#blogpost3" title="TCSA will be at the Welsh Super Cup">TCSA will be at the Welsh Super Cup</a></h3>
			<p class="blog-summary">See us and our teams at the Welsh Super Cup this week with over 10 events</p>
		</article>
	</article>
	
</section>