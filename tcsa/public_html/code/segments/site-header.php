
<div id="headstrip" class="section-container">

	<header id="pagetop">
		
		<div class="w960 clearfix">		  		
			
			<hgroup class="left">
				<h1 id="sitelogo"><a href="<? echo $home_url; ?>" title="<? echo $home_title; ?>"><? echo $compname; ?></a></h1>
				<h2 class="strapline hex1"><? echo $strapline; ?></h2>
				<h3 class="sponsor-plate"><span class="sponsor-ident">Sponsored By <? echo $sponsor_name; ?></span></h3>
			</hgroup>
			
			<hr />
				  
			<nav id="pagetop-menu" class="left">						
				<ul class="menu-container mainmenu clearfix">
					<?php include("code/segments/site-menu.php"); ?>
				</ul>		 
			</nav>
				
		</div>
	
	</header>
	
</div>