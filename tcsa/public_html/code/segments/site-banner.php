<?php
		$page = explode("/", $_SERVER['REQUEST_URI']);
		$page = $page[count($page) - 1];
		switch($page) {
				case "":
				case "index.php":
		?>
		
			<section id="BANNER" class="section-container">
	
				<article id="home-banner">
				
					<div class="banner-patternfx topstrip"></div>
					
					<div class="banner-message-block w960">
						<div class="overlay-screen clearfix">
							<hgroup class="banner-text">
								<h1><? echo $bannertitle1; ?></h1>
								<h2><? echo $bannerintro1; ?> <a href="<? echo $page_url4; ?>" title="<? echo $page_title4; ?>" class="btn btn-style1 cta1"><span>Download TCSA</span>FREE eBook</a></h2>
							</hgroup>
														
						</div>
					</div>
					
					<hr />
					
					<div class="banner-patternfx bottomstrip">
						<div id="newsbar" class="w960 clearfix">
							<div class="icon-cal left"></div>
							<ol class="newsticker left">
								<li class="post"><a href="/news1.php" title="Read more">'Pathway to Success' 18th &amp; 19th August 2012 the official TCSA launch Event.</a></li>
								<li class="post"><a href="/news2.php" title="Read more">Get ready for the start of the new football season Saturday 18th August 2012.</a></li>
								<li class="post"><a href="/news3.php" title="Read more"><? echo $compname; ?> at the Welsh Super Cup July 23rd 2012.</a></li>
							</ol>		
							<?php include("code/snippets/social-links.php"); ?>		
						</div>
					</div>	
						
				</article>
					
			</section>
		
		<?php
				break;			
				default :
		?>
		
			<section id="BANNER" class="section-container banner-default">
	
				<article id="page-banner">
				
					<div class="banner-patternfx topstrip"></div>			
					<div class="banner-patternfx bottomstrip"></div>	
						
				</article>
					
			</section>	
			
<?php }?>