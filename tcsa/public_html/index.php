<?php include("code/snippets/doc-head.php"); ?>

<body>

<?php include("code/snippets/ie6-support.php"); ?>
  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<hr>
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head"></div>
	  
		<!-- Home Section Blocks	--> 
		<div class="section-row-container">
			<?php include("code/snippets/blocks-home.php"); ?>
		</div>
		<!-- Home Section Blocks END -->		
		
		<hr>
		
		<!-- Main Content Section -->
		<div class="section-row-container row-highlight ltr">		
			<section id="tcsa-section1" class="w960">				
				<article class="boxcontainer promovid clearfix">
					<header class="boxcontent">
						<hgroup class="boxintro">
							<h2 class="boxheadline">Inspiring young players to excel and win in soccer and in life.</h2>
							<h3 class="boxblurb">Whether an aspiring pro or playing for the pure love of the game, our programmes are proven to help children become more confident, skilful, faster, stronger, healthier and happier.</h3>
						</hgroup>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Enquire about a placement for your child" class="btn btn-style1 cta1"><span>Find out if you qualify</span></a></p>
					</header>
					
					<figure class="boxpic bgfx">
						<video width="530" height="300" controls poster="/assets/vid/showcase-cover.jpg" class="videotag imgframe">
						  <source src="assets/vid/promo.mp4" type="video/mp4" />
						  <p class="novid-label">Your browser does not support the video tag.</p>
						</video>
						<?php /*?><p class="btn-container"><a href="#" title="Share Video" class="btn">Share</a> <a href="#" title="Tweet Video" class="btn tweetbtn"><span>Tweet</span></a></p><?php */?>
					</figure>
				</article>				
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section2" class="w960 ltr">				
				<article class="boxcontainer clearfix">
					
					<header class="boxcontent">
						<hgroup class="boxintro">
							<h2 class="boxheadline">Personal and professional development training.</h2>
							<h3 class="boxblurb">We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.</h3>
						</hgroup>
						<p class="btn-container"><a href="<? echo $page_url2; ?>" title="<? echo $page_title2; ?>" class="btn">View Our Courses</a> <a href="<? echo $page_url4; ?>" title="<? echo $page_title4; ?>" class="btn btn-style1">Download our Free eBook</a></p>
					</header>
					
					<figure class="boxpic bgfx">
						<span class="imgstacked">
							<img src="assets/img/programmes/winning-attitude.jpg" width="340" height="210" alt="Welsh Super Cup 2012"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption>TCSA Under 13's at the Welsh Super Cup 2012</figcaption>
					</figure>
										
				</article>				
			</section>			
		</div>
		
		<hr>		
		
		<div class="section-row-container last">
			<!-- Page Block Two -->						   
			<?php include("code/snippets/blocks-one.php"); ?>												   
			<!-- Page Block Two END -->	
		</div>		
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include("code/snippets/js-scripts.php"); ?>
  
</body>
</html>