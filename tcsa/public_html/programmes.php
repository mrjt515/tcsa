<?php include ("code/snippets/doc-head.php"); ?>

<body class="two-column-layout">

<?php include( "code/snippets/ie6-support.php"); ?>
  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $nav_menu2; ?></h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<?php include("code/snippets/menu-services.php"); ?>
					</ul>					
					<div class="contentbox section-intro">
						<p>We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs. Your child will benefit from a combined 100 + years of coaching experience from grass-roots to elite level.</p>
					</div>					
				</article>					
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section2" class="w960 ltr">			
				<article id="prog1" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_1; ?></h3>
						<h4>- Holiday and Weekly Training -</h4>						
						<p>This programme focuses primarily on the technical, tactical, physical, mental and social aspects of development through the medium of football-specific sessions. We work in smaller groups in comparison to many other ‘soccer schools’, with a maximum Coach: Player ratio of 1:10.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>
					<figure class="sideB">
						<span class="imgstacked"><img src="/assets/img/programmes/elite.jpg" width="340" height="210" alt="<? echo $compname; ?> <? echo $service_menu_1; ?>"/>
						<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
						<figcaption class="hide"><? echo $compname; ?> <? echo $service_menu_1; ?></figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section3" class="w960 rtl">			
				<article id="prog2" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_2; ?> Programme</h3>
						<h4>- Master The Ball, Master The Game -</h4>						
						<p>This programme ‘does exactly what it says on the tin’ – helps to accelerate the skill development of young players’ football skills in a way that is almost impossible to do in group sessions. How? By giving the individual player more attention on his unique learning requirements physically, mentally, technically and tactically.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked"><img src="/assets/img/programmes/prog-skills-accelerator.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
						<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_2; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section4" class="w960 ltr">			
				<article id="prog3" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_3; ?> Programme</h3>
						<h4>- Before Confidence Comes Courage -</h4>						
						<p>This is currently a 1-on-1 coaching programme and gives young players the skills to become more assertive, expressive and confident on and off the field. So before confidence comes courage, and courage is something that all our players are encouraged to develop week-in and week-out, like a body builder develops his muscles.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked">
							<img src="/assets/img/programmes/prog-confident-player.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_3; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section5" class="w960 rtl">			
				<article id="prog4" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_4; ?> Programme</h3>
						<h4>- Before Confidence Comes Courage -</h4>						
						<p>This is currently a 1-on-1 coaching programme and gives young players the skills to become more assertive, expressive and confident on and off the field. So before confidence comes courage, and courage is something that all our players are encouraged to develop week-in and week-out, like a body builder develops his muscles.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked">
							<img src="assets/img/programmes/winning-attitude.jpg" width="340" height="210" alt="<? echo $compname; ?> <? echo $service_menu_4; ?> Programme"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_4; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section6" class="w960 ltr">			
				<article id="prog5" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_5; ?> Programme</h3>
						<h4>- Before Confidence Comes Courage -</h4>						
						<p>Players who are playing representative football (schoolboys, reps, boys’ clubs, development centres) and have the aim of being signed by a professional club, Players who have been released by professional clubs and want to get back into the same or another professional club.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked">
							<img src="/assets/img/programmes/proprogramme.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_5; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section7" class="w960 rtl">			
				<article id="prog6" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_6; ?> Programme</h3>
						<h4>- Empowers learning, confidence and happiness -</h4>						
						<p>A live group learning format for parents who want to support their children in a way that maximizes their learning, enjoyment and ability to excel in football and life.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked">
							<img src="/assets/img/programmes/parent.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_6; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section8" class="w960 ltr">			
				<article id="prog7" class="contentbox clearfix">			
					<header class="sideA">
						<h3><? echo $service_menu_7; ?> Programme</h3>
						<h4>- Before Confidence Comes Courage -</h4>						
						<p>This is currently a 1-on-1 coaching programme and gives young players the skills to become more assertive, expressive and confident on and off the field. So before confidence comes courage, and courage is something that all our players are encouraged to develop week-in and week-out, like a body builder develops his muscles.</p>
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1">Fill out a Pre-qualify form</a> <?php /*?><a href="#" title="Learn More" class="btn">Learn More</a><?php */?></p>
					</header>					
					<figure class="sideB">
						<span class="imgstacked">
							<img src="/assets/img/programmes/nutrition.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span>
						</span>
						<figcaption><? echo $compname; ?> <? echo $service_menu_7; ?> Programme</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>		
		
		<div class="section-row-container">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
					<p>If any of these programmes interest you and your child then why not fill out a pre-qualify form with us today.</p>			
					<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
					</article>							
			</section>			
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>