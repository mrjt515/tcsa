<?php include ("code/snippets/doc-head.php"); ?>

<body id="page-privacypol" class="default-layout fullwidth-layout TandCs">

<?php include( "code/snippets/ie6-support.php"); ?>


  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">Privacy Policy</h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<li><a href="#ppsect1" title="Web browser cookies">Web browser cookies</a></li>
						<li><a href="#tcsa-section3" title="How we use collected information">How we use collected information</a></li>
						<li><a href="#tcsa-section4" title="Third party websites">Third party websites</a></li>
						<li><a href="#tcsa-section5" title="Contacting us">Contacting us</a></li>
					</ul>					
					<div class="contentbox section-intro">
						<p>This Privacy Policy governs the manner in which <? echo $compname; ?> collects, uses, maintains and discloses information collected from users (each, a "User") of the <? echo $site_url; ?> website ("Site"). This privacy policy applies to the Site and all products and services offered by <? echo $compname; ?></p>
						
					</div>					
				</article>					
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section2" class="w960 ltr">			
				<article class="contentbox clearfix">			
					<h4>Personal Identification Information</h4>
					<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>					
					<h4 id="ppsect1">Web browser cookies</h4>
					<p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>										
				</article>							
			</section>						
		</div>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section3" class="w960 ltr">			
				<article class="contentbox clearfix">			
					<h4>How we use collected information</h4>
					<p>The Champion's Soccer Academy may collect and use Users personal information for the following purposes:</p>
					<ol>
						<li><strong>To improve customer service</strong><br>
							Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>
						<li><strong>To send periodic emails</strong><br>
						We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</li>
					</ol>
					<h4>How we protect your information</h4>
					<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>
												
					<h4>Sharing your personal information</h4>
					<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>								
				</article>							
			</section>						
		</div>
		
		<div class="section-row-container">			
			<section id="tcsa-section4" class="w960 ltr">			
				<article class="contentbox clearfix">			
					<h4>Third party websites</h4>
					<p>Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website's own terms and policies.</p>
					<h4>Changes to this privacy policy</h4>
					<p>The Champion's Soccer Academy has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
					<h4>Your acceptance of these terms</h4>
					<p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>								
				</article>							
			</section>						
		</div>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section5" class="w960 ltr">			
				<article class="contentbox clearfix">			
					<h4>Contacting us</h4>
					<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br><br>
					<strong><? echo $compname; ?></strong><br>
					<? echo $address; ?><br><br>
					Tel. <strong><? echo $comptel; ?></strong><br>
					<a href="mailto:<? echo $compemail; ?>"><? echo $compemail; ?></a><br>
					<? echo $site_url; ?><br><br>
					<small><em>This document was last updated on <? echo $policydate; ?></em></small>
					</p>								
				</article>							
			</section>						
		</div>
		
		
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>