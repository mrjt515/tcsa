<?php include ("code/snippets/doc-head.php"); ?>

<body class="two-column-layout">

<?php include( "code/snippets/ie6-support.php"); ?>


  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $nav_menu1; ?></h2>				
				<?php include("code/snippets/cta-btn.php"); ?>	
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
				
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<li><a href="#awarnessguide" title="Consumer Awarness Guide">Consumer Awarness Guide</a></li>
						<?php /*?><li><a href="#tutvideo1" title="Keep Up Video Tutorial">Keep Up Video Tutorial</a></li>
						<li><a href="#introtoNLP" title="An Introduction to NLP">An Introduction to NLP</a></li><?php */?>
					</ul>					
					<div class="contentbox section-intro">
						<p>We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.</p>
					</div>					
				</article>					
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section2" class="w960 rtl">			
				<article id="awarnessguide" class="contentbox clearfix">			
					<header class="sideA">
						<h3>Consumer Awarnes Guide</h3>
						<h4 class="format-icon pdf">- Book in PDF format -</h4>						
						<p>Read all about The Champion’s Soccer Academy philosophy and why we believe we are the market leaders in junior and youth football caoching using the latest NPL techniques.</p>
						<p class="btn-container"><a href="<? echo $page_url4; ?>" title="Download the Consumer Awarnes Guide eBook" class="btn btn-style1 cta1">Download eBook</a></p>
						<small><em>*Registration Required, you will receive an email with a link to download</em></small>
					</header>
					<figure class="sideB">
						<img src="assets/img/ebook-consumerguide.png" width="280" height="360" alt="<? echo $compname; ?> Consumer Awarnes Guide"/>
						<figcaption class="hide"><? echo $compname; ?> Consumer Awarnes Guide</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<?php /*?><hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section3" class="w960 rtl">			
				<article id="introtoNLP" class="contentbox clearfix">			
					<header class="sideA">
						<h3>An Introduction to NLP</h3>
						<h4 class="format-icon pdf">- eBook in PDF format -</h4>						
						<p>Read all about how The Champion’s Soccer Academy integrates the latest NLP techniques into each programme to great effect in your childs development in life skills.</p>
						<p class="btn-container"><a href="<? echo $page_url4; ?>" title="Download An Introduction to NPL eBook" class="btn btn-style1 cta1">Download eBook</a></p>
						<small><em>*Registration Required, you will receive an email with a link to download</em></small>
					</header>
					<figure class="sideB">
						<img src="/assets/img/ebook-npl.png" width="280" height="360" alt="<? echo $compname; ?> An Introduction to NLP"/>
						<figcaption class="hide"><? echo $compname; ?> An Introduction to NPL</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section4" class="w960 rtl">			
				<article id="tutvideo1" class="contentbox clearfix">			
					<header class="sideA">
						<h3>Keep Up Skill Technique</h3>
						<h4 class="format-icon vid">- Soccer Skill Video Tutorial -</h4>						
						<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
						<p class="btn-container"><a href="#" title="Download the Soccer Skill Video Tutorial" class="btn btn-style1 cta1">Download Video</a></p>
						<small><em>*Registration Required, you will receive an email with a link to view video</em></small>
					</header>
					<figure class="sideB">
						<img src="/assets/img/image-dummy.jpg" width="280" height="173" alt="<? echo $compname; ?> Soccer Skill Video Tutorial"/>
						<figcaption class="hide"><? echo $compname; ?> Soccer Skill Video Tutorial</figcaption>
					</figure>													
				</article>							
			</section>						
		</div><?php */?>
		
		<hr>		
				
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>