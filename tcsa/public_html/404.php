<!doctype html>
<?php include("code/site-config.php"); ?>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Sorry Page Not Found | <? echo $compname; ?></title>
  <meta name="author" content="<? echo $designer; ?>">

  <link rel="stylesheet" href="code/css/global.css">

  <script src="/code/js/libs/modernizr-2.5.3.min.js"></script> 
  
  <!-- Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/code/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  
  <?php include("code/snippets/analytics.php"); ?>
   
</head>

<body id="page404" class="default-layout">

<?php include( "code/snippets/ie6-support.php"); ?>


  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<hr>
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">Sorry Page Not Found</h2>
				<a href="/contact.php" title="Get In Touch" class="btn btn-style1 cta2"><span>Get In Touch</span></a>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<?php include("code/snippets/menu-services.php"); ?>
					</ul>					
					<div class="contentbox section-intro">
						<p>Oops!, Sorry but the page you are looking for is no longer available or has been moved to another section of this site.<br><br>
						<a href="<? echo $home_url; ?>" title="<? echo $home_title; ?>" class="btn btn-style1"><? echo $home_title; ?></a><br><br></p>						
					</div>					
				</article>					
			</section>			
		</div>
			
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>