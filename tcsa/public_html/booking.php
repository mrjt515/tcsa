<?php include ("code/snippets/doc-head.php"); ?>

<body class="two-column-layout">

<?php include( "code/snippets/ie6-support.php"); ?>


  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $nav_menu3; ?></h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->		
		
		<div class="section-row-container">			
			<section id="tcsa-section1" class="w960 clearfix">			
				
				<article class="contentbox col-1">			
					
					<header>
						<h2 class="page-caption">Invest in your child's potential and join the TCSA, we are more than just football coaches.</h2>
						<p>To find out more on how you qualify for acceptance for <? echo $compname; ?> then fill out our pre-qualify registration form below.</p>
					</header>
					
					<?php include("code/snippets/form-booking.php"); ?>
													
				</article>
				
				<aside class="sidebar col-2">
					<?php include("code/segments/site-sidebar.php"); ?>
				</aside>
											
			</section>					
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>