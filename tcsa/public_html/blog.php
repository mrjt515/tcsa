<?php include ("code/snippets/doc-head.php"); ?>

<body id="pageBLOG" class="two-column-layout blog-layout">

<?php include( "code/snippets/ie6-support.php"); ?>
  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $news_menutitle; ?></h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			
			<section id="tcsa-section1" class="w960 clearfix blogposts">				
				<article class="col-1">					
					
					<article id="blogpost1" class="contentbox clearfix">
						<h3><? echo $compname; ?> Official Launch</h3>
						<h4 class="datestamp"><span>Posted:</span> August 17th 2012</h4>
						<figure class="blogimage">
							<span class="imgstacked"><img src="/assets/img/news/launch.jpg" width="500" height="210" alt="Get Ready For the New Season"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
							<figcaption><? echo $compname; ?> Official Launch</figcaption>
						</figure>						
						<p>As part of The Champion's Soccer Academy official launch week, we will be holding a series of Parent Workshops aiming to showcase our coaching philosophy.</p>
						<p class="btn-container"><a href="/news1.php" title="Read More" class="btn">Read More</a></p>
					</article>	
					
					<article id="blogpost2" class="contentbox clearfix">
						<h3>Get Ready For the New Football Season</h3>
						<h4 class="datestamp"><span>Posted:</span> August 17th 2012</h4>
						<figure class="blogimage">
							<span class="imgstacked"><img src="/assets/img/news/newseason.jpg" width="500" height="210" alt="Get Ready For the New Football Season"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
							<figcaption>Get Ready For the New Football Season</figcaption>
						</figure>						
						<p>With the new football season about to kick off we would like to get you ready by offering a series of workshops in conjunction with our official launch week starting Saturday 18th August 2012.</p>
						<p class="btn-container"><a href="/news2.php" title="Read More" class="btn">Read More</a></p>																		
					</article>
				
					<article id="blogpost3" class="contentbox clearfix">
						<h3><? echo $compname; ?> at the Welsh Super Cup</h3>
						<h4 class="datestamp"><span>Posted:</span> July 23rd 2012</h4>
						<figure class="blogimage">
							<span class="imgstacked"><img src="/assets/img/news/welshsupercup.jpg" width="340" height="210" alt="<? echo $compname; ?> at the Welsh Super Cup"/>
							<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
							<figcaption><? echo $compname; ?> at the Welsh Super Cup</figcaption>
						</figure>					
						<p>The Champion's Soccer Academy attended the Welsh Super Cup 2012 with the Under 13's and Under 10's team, see how they got on...</p>
						<p class="btn-container"><a href="/news3.php" title="Read More" class="btn">Read More</a></p>																	
					</article>	
							
				</article>
				
				<aside class="sidebar col-2">
					<?php include("code/segments/site-sidebar.php"); ?>
				</aside>
									
			</section>
		</div>
		
		
		<hr>
		
		<div class="section-row-container row-highlight">
			<!-- Page Block Two -->						   
			<?php include("code/snippets/blocks-two.php"); ?>												   
			<!-- Page Block Two END -->	
		</div>
		
		<hr>		
		
		<div class="section-row-container">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
					<p>If any of these programmes interest you and your child then why not book a trial session with us today.</p>			
					<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Book a trial session today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
					</article>							
			</section>			
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>