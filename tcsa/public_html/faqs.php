<?php include ("code/snippets/doc-head.php"); ?>

<body id="pageFAQs" class="two-column-layout">

<?php include( "code/snippets/ie6-support.php"); ?>


  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $faqs_menutitle; ?></h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix first">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your child’s future.</h2>
					</header>									
					<div class="contentbox section-intro">
						<p>Here are some of the questions members have asked us and the answers we gave helped to decide the best approach for introducing their child to our soccer programmes.</p>
											
					</div>
					<ul class="linklist">
						<li><a href="#q1" title="Is my child too young?">Is my child too young?</a></li>
						<?php /*?><li><a href="#q2" title="How can I pay?">How can I pay?</a></li><?php */?>
						<li><a href="#q3" title="Do you have programmes for girls?">Do you have programmes for girls?</a></li>
						<li><a href="#q4" title="How do I know what programme is best suited to my child’s ability?">What programme is best suited to my child’s ability?</a></li>
						<li><a href="#q5" title="How can parents get involved?">How can parents get involved?</a></li>
						<?php /*?><li><a href="#q6" title="How long does a programme last?">How long does a programme last?</a></li>
						<li><a href="#q7" title="What happens when my child has completed a programme?">What happens when my child has completed a programme?</a></li>
						<li><a href="#q8" title="How can I cancel my membership?">How can I cancel my membership?</a></li><?php */?>
					</ul>				
				</article>					
			</section>			
		</div>
		
		<div class="section-row-container row-highlight">
			<section id="tcsa-section1-2" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Need to ask a question?</h3>
					<p>Not a question here that you need answering then just ask us and we will give you the answer.</p>
						<p class="btn-container"><a href="mailto:<? echo $compemail5; ?>?Subject=I would like to ask a question about TCSA soccer programmes" title="Ask us a question" class="btn btn-style1 cta1">Ask us a question</a></p>
					</article>							
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section2" class="w960 faqlist">			
				<article id="q1" class="contentbox clearfix">			
					<header>
						<h3>Is my child too young?</h3>
					</header>
					<footer>
						<p>We have two teams Under 13's and Under 10's, for more information about the criteria then you can read more in our <a href="ebook-download.php" title="Download our FREE report">FREE Report</a>.</p>
					</footer>								
				</article>
				<?php /*?><article id="q2" class="contentbox clearfix">			
					<header>
						<h3>How can I pay?</h3>
					</header>
					<footer>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>
					</footer>								
				</article><?php */?>
				<article id="q3" class="contentbox clearfix">			
					<header>
						<h3>Do you have programmes for girls?</h3>
					</header>
					<footer> 
						<p>We like to encourage all children to take up soccer as it's a great from of learning and fitness. We aim to work towards a programme for girls in the future so would like more girls to take up interest then <a href="ebook-download.php" title="Download our FREE report">download</a> our <strong>FREE</strong> report.</p>
					</footer>								
				</article>
				<article id="q4" class="contentbox clearfix">			
					<header>
						<h3>What programme is best suited to my child’s ability?</h3>
					</header>
					<footer>
						<p>Each child has different levels of learning ability and skills so we assess them correctly in accordance to their ability. For an in-depth look at how we manage child's abilities <a href="ebook-download.php" title="Download our FREE report">download</a> our <strong>FREE</strong> report.</p>
					</footer>								
				</article>
				<article id="q5" class="contentbox clearfix">			
					<header>
						<h3>How can parents get involved?</h3>
					</header>
					<footer>
						<p>Firstly, parents are critical in the success of their child from encouragement and wanting their child to achieve their maximum potential like all parents we want what's best for our children. For an in-depth look at how parents play a key role <a href="ebook-download.php" title="Download our FREE report">download</a> our <strong>FREE</strong> report.</p>
					</footer>								
				</article>
				<?php /*?><article id="q6" class="contentbox clearfix">			
					<header>
						<h3>How long does a programme last?</h3>
					</header>
					<footer>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>
					</footer>								
				</article>
				<article id="q7" class="contentbox clearfix">			
					<header>
						<h3>What happens when my child has completed a programme?</h3>
					</header>
					<footer>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>
					</footer>								
				</article>
				<article id="q8" class="contentbox clearfix">			
					<header>
						<h3>How can I cancel my membership?</h3>
					</header>
					<footer>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>
					</footer>								
				</article>	<?php */?>												
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
					<p>Have these answers ignited the interest of you and your child then why not book a trial session with us today.</p>			
					<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Book a trial session today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
					</article>							
			</section>			
		</div>	
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>