<?php include ("code/snippets/doc-head.php"); ?>

<body id="pageBLOG" class="two-column-layout blog-layout">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php include( "code/snippets/ie6-support.php"); ?>
  
<div id="WRAPPER">
		
	<!-- Header Section -->
	<?php include("code/segments/site-header.php"); ?>
	<!-- Header Section END -->	
	
	<!-- Main Banner Slider	 -->	  		  	
	<?php include("code/segments/site-banner.php"); ?>
	<!-- Main Banner Slider END -->	 
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo $news_menutitle; ?></h2>
				<?php include("code/snippets/cta-btn.php"); ?>
				<?php include("code/snippets/social-links.php"); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section section id="tcsa-section1" class="w960 clearfix blogposts">				
					
					<article class="col-1">
					
						<article id="blogpost1" class="contentbox">
							<header>
								<h3><? echo $compname; ?> at the Welsh Super Cup</h3>
								<h4 class="datestamp"><span>Posted:</span> August 17th 2012</h4>
								<figure class="blogimage">
									<span class="imgstacked"><img src="/assets/img/news/welshsupercup.jpg" width="500" height="210" alt="<? echo $compname; ?> at the Welsh Super Cup"/>
									<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
									<figcaption><? echo $compname; ?> at the Welsh Super Cup</figcaption>
								</figure>						
								<p>Back from our participation at the Welsh Super Cup and it was a baptism of fire! Both teams gave away a lot in age, size and experience to their opposition. The U13's played some excellent football in patches. Overall, it was a steep learning curve for the boys, which is understandable as it was their first competitive game together. </p>
								<p>The Under 10's played an excellent team from Birmingham. Outplayed for 3/4's of the game, the boys stuck at it, never giving in for a second. Then really started to find a bit of rhythm in the last quarter, created quite a few chances and would have scored a few goals if it wasn't for excellent goal-keeping...So an excellent day for learning. Now the challenge is to integrate it!</p>
							</header>
							<footer>
								<p class="btn-container"><a href="/blog.php" title="back to Blog" class="btn">Return To Blog</a></p>
							
								<div class="shareplate clearfix">
									<h4>Share this page</h4>						
									<div class="g-plusone" data-size="medium" data-annotation="none" data-href="http://<? echo $site_url; ?>"></div>
									<script type="text/javascript">
									  window.___gcfg = {lang: 'en-GB'};
									
									  (function() {
										var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
										po.src = 'https://apis.google.com/js/plusone.js';
										var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
									  })();
									</script>
									<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<? echo $site_url; ?>" data-count="none">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
									<div class="fb-like" data-href="http://<? echo $site_url; ?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
								</div>
								
							</footer>
						</article>
						
						<article id="fbcomment-block" class="contentbox clearfix ">			
							<h4>Comments</h4>
							<div class="fb-comments" data-href="http://www.thechampionssocceracademy.com" data-num-posts="10" data-width="600"></div>													
						</article>
					
					</article>
					
					<aside class="sidebar col-2">
						<?php include("code/segments/site-sidebar.php"); ?>
					</aside>
										
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	<!-- Footer Section -->
	<?php include("code/segments/site-footer.php"); ?>
	<!-- Footer Section END -->
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php include ("code/snippets/js-scripts.php"); ?>
  
</body>
</html>