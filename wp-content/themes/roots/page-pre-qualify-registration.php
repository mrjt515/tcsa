<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Pre-Qualify Registration' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
?>
	
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
				<?php get_template_part('templates/cta', 'btn'); ?>
				<?php get_template_part('templates/social', 'links'); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->		
		
		<div class="section-row-container">			
			<section id="tcsa-section1" class="w960 clearfix">			
				
				<article class="contentbox col-1">			
					
					<header>
						<h2 class="page-caption">Invest in your child's potential and join the TCSA, we are more than just football coaches.</h2>
						<p>To find out more on how you qualify for acceptance for <? echo $compname; ?> then fill out our pre-qualify registration form below.</p>
					</header>
					
					<?php get_template_part('templates/form', 'booking'); ?>
													
				</article>
				
				<aside class="sidebar col-2">
					<div class="sideblock item1">
						<?php get_template_part('templates/sidebar', 'pre-qualify-registration'); ?>
					</div>
				</aside>
											
			</section>					
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	
<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  
