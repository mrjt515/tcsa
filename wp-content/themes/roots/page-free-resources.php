<?php get_template_part('templates/head'); ?>


<body class="two-column-layout">
  
		
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">Free Resources</h2>				
				<?php get_template_part('templates/cta', 'btn'); ?>	
				<?php get_template_part('templates/social', 'links'); ?>				
			</section>	
		</div>
				
		<!-- Main Content Section -->

		<?php get_template_part('templates/template', 'free-resources') ?>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section2" class="w960 rtl">			
				<article id="awarnessguide" class="contentbox clearfix">			
					<header class="sideA">
						<h3>Consumer Awarnes Guide</h3>
						<h4 class="format-icon pdf">- Book in PDF format -</h4>						
						<p>Read all about The Champion’s Soccer Academy philosophy and why we believe we are the market leaders in junior and youth football caoching using the latest NPL techniques.</p>
						<p class="btn-container"><a href="/e-book-download/" title="Download the Consumer Awarnes Guide eBook" class="btn btn-style1 cta1">Download eBook</a></p>
						<small><em>*Registration Required, you will receive an email with a link to download</em></small>
					</header>
					<figure class="sideB">
						<img src="../../assets/img/ebook-consumerguide.png" width="280" height="360" alt="<? echo $compname; ?> Consumer Awarnes Guide"/>
						<figcaption class="hide"><? echo $compname; ?> Consumer Awarnes Guide</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<?php /*?><hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section3" class="w960 rtl">			
				<article id="introtoNLP" class="contentbox clearfix">			
					<header class="sideA">
						<h3>An Introduction to NLP</h3>
						<h4 class="format-icon pdf">- eBook in PDF format -</h4>						
						<p>Read all about how The Champion’s Soccer Academy integrates the latest NLP techniques into each programme to great effect in your childs development in life skills.</p>
						<p class="btn-container"><a href="<? echo $page_url4; ?>" title="Download An Introduction to NPL eBook" class="btn btn-style1 cta1">Download eBook</a></p>
						<small><em>*Registration Required, you will receive an email with a link to download</em></small>
					</header>
					<figure class="sideB">
						<img src="/assets/img/ebook-npl.png" width="280" height="360" alt="<? echo $compname; ?> An Introduction to NLP"/>
						<figcaption class="hide"><? echo $compname; ?> An Introduction to NPL</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section4" class="w960 rtl">			
				<article id="tutvideo1" class="contentbox clearfix">			
					<header class="sideA">
						<h3>Keep Up Skill Technique</h3>
						<h4 class="format-icon vid">- Soccer Skill Video Tutorial -</h4>						
						<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
						<p class="btn-container"><a href="#" title="Download the Soccer Skill Video Tutorial" class="btn btn-style1 cta1">Download Video</a></p>
						<small><em>*Registration Required, you will receive an email with a link to view video</em></small>
					</header>
					<figure class="sideB">
						<img src="/assets/img/image-dummy.jpg" width="280" height="173" alt="<? echo $compname; ?> Soccer Skill Video Tutorial"/>
						<figcaption class="hide"><? echo $compname; ?> Soccer Skill Video Tutorial</figcaption>
					</figure>													
				</article>							
			</section>						
		</div><?php */?>
		
		<hr>		
				
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
	  	



<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  
</body>
</html>