<?php
/**
 * Custom functions
 */



function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "/wp-content/themes/roots/assets/img/banner/slide-1.jpg";
  }
  return $first_img;
}



add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 150, 150, true );
add_image_size( 'my_thumbnail', 40, 40, true );


function is_child_page($page = NULL){
    global $post;
    if ($page == NULL){
        $p = get_post($post->ID);
        if ($p->post_parent  > 0 ){
            return true;
        }else{
            return false;
        }
    }
    $args = array( 'child_of' => (int)$page);
    $pages = get_pages($args); 
    foreach ($pages as $p){
        if ($p->ID = $post->ID){
            return true;
            break;
        }
    }
    return false;
}

// deliver the same theme for programms & children of programmes (pages)
add_filter( 'page_template', 'tcsa_set_theme' );
function tcsa_set_theme( $templates = '' )
{
  global $post;

  if( //$post->ID == 23 || 
    $post->post_parent == 23 )
  {
    $templates = locate_template('templates/template-programmes-children.php');
  }

  return $templates;


}

add_filter( 'page_template', 'tcsa_set_theme_fixtures' );
function tcsa_set_theme_fixtures( $templates = '' )
{
  global $post;

  if( //$post->ID == 23 || 
    $post->post_parent == 327 )
  {
    $templates = locate_template('templates/template-fixtures-results-children.php');
  }

  return $templates;

}

add_action('save_post', 'new_menu_item');
function new_menu_item($ID) {
  $page = get_page($ID);
  if($page->post_parent != 23) return;
}

function exclude_category($query) {
if ( $query->is_home() ) {
$query->set('cat', '-16');
}
return $query;
}
add_filter('pre_get_posts', 'exclude_category');












?>