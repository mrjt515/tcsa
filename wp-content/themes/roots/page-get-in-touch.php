

<body class="two-column-layout">


		
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">Contact Us</h2>
				<?php get_template_part('templates/cta', 'btn'); ?>	
				<?php get_template_part('templates/social', 'links'); ?>	
			</section>	
		</div>
		
		<!-- Main Content Section -->		
		
		<div class="section-row-container">			
			<section id="tcsa-section1" class="w960 clearfix">			
				
				<article class="contentbox col-1">			
					
					<header>
						<h2 class="page-caption">Invest in your child's potential and join the TCSA, we are more than just football coaches.</h2>
						<p>If you would like more information about The Champion's Soccer Academy and our soccer programmes please get in touch and a member of our team will be on hand to assist you.</p>
					</header>
					
					<?php 
						$completed = '';
					if($completed == false) {?>				
				
					<!-- Contact Form -->						
					<?php get_template_part('templates/form', 'contact') ?>					<!-- Contact Form END -->			
				
				<?php } else {
				
				echo <<<HTML

				<h3 class="form-title success">Thank you for your Enquiry</h3>
				<p>Your enquiry has been sent and we will get back to you as soon as we can.</p>
				
HTML;
					
					  }?>
													
				</article>
				<aside class="sidebar col-2">
					<div class="sideblock mapbox item1">
						<h4 class="title t-map">
							Map
							<br>
							<span>Find where we are</span>
						</h4>
						<a href="http://goo.gl/maps/lt1LQ" title="View Map" target="_blank">
							<img src="/assets/img/tcsa-map.png" width="240" height="170" alt="Map">
						</a>
						<div class="contact-wrap">
							<p>
								<span class="smalltxt">Main Office:</span>
							</p>
							<h5>
								<span class="address-tag">
									<em>The Champion's Soccer Academy</em>
									<br>
									32 Wern Road, 
									<br>
									Swansea, 
									<br>
									United Kingdom, 
									<br>
									SA1 2PA
								</span>
							</h5>
							<a href="http://goo.gl/maps/lt1LQ" title="View Map" class="btn btn-style1">View Map</a>
						</div>
					</div>
					<div class="sideblock item2">
						<h4 class="title t-contact">
							Get in Touch
							<br>
							<span>Speak to one of our Coaches</span>
						</h4>
						<div class="contact-wrap">
							<p>
								<span class="smalltxt">Call us on:</span>
								<strong>01792 421 600</strong>
							</p>
							<h5>
								<em>Mon - Fri:</em>
									9am - 5pm
								<em>Sat:</em>
									10am - 2pm
							</h5>
							<a href="mailto:info@thechampionssocceracademy.com?Subject=I would like more information about TCSA" title="Send us an Email" class="btn btn-email">
								<span>Send us an Email</span>
							</a>
						</div>
					</div>
					<div class="sideblock item3">
						<ul class="coach-idtag clearfix">
							<li class="coach-avatar">
								<img src="/assets/img/avatar-frankie-burrows.png" width="100" height="100" alt="Frankie Burrows">
							</li>
							<li class="coach-label">
								<span class="staff-title">Head Coach</span>
								<br>
								<span class="staff-name">Frankie Burrows</span>
								<br>
								<a href="mailto:frankie@thechampionssocceracademy.com?Subject=Message:" title="Send a Message to Frankie Burrows" class="btn btn-style1 btnmessage">
									Send a Message
								</a>
							</li>
						</ul>
					</div>
				</aside>	
			</section>					
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   

	  	



<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  
</body>
</html>