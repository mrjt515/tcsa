<?php /*This is actually the blog page but due to the way that wordpress functions, in order for the file and it's contents
to be displayed the file must be called home.php*/?>

<?php get_template_part('templates/head'); ?>

<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Blog' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
?>

<div id="mainsection" role="main">

	<div class="section-row-container pagetitle-head">
		<section id="tcsa-section0" class="w960 clearfix">				
			<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
			<?php get_template_part('templates/cta', 'btn'); ?>	
			<?php get_template_part('templates/social', 'links'); ?>		
		</section>	
	</div>




		
	<div class="section-row-container">			
		<section id="tcsa-section1" class="w960 clearfix blogposts">
			<article class="col-1">											 

			<?php while (have_posts()) : the_post(); ?>
			
				<header>
					<h2 id="blog-title" class="entry-title">
						<?php the_title(); ?>
					</h2>

					<div class="date">
						<div class="display-date">
							<? get_template_part('templates/entry', 'meta'); ?>
						</div>																    		
							<?php
								if (has_post_thumbnail()) {
									?>
									<span class="imgstacked">
									<span class="imgstack l1"></span><span class="imgstacked l2"></span>
									<?
									the_post_thumbnail('large');
								}
							?>
					</div>
				</header>
				
				<br>
				<br>
				
				<div class="entry-summary">
				<!--Just before where the 'Continued' link is being created-->
			    <? the_excerpt(); ?>
				</div>

				<br/>								 
				<p class="btn-container"><a href="<?php the_permalink(); ?>" title="Read More" class="btn">Read More</a></p>
			<br>
			<br>
			<hr/>
			<?endwhile?>
			</article>

			<aside class="sidebar col-2">
				<div class="sideblock item1">
					<?php get_sidebar('sidebar', 'primary'); ?>
				</div>
				<div class="sideblock item2 no-border">
					<div class="product-adblock">
						<img src="/assets/img/ebook-consumerguide.png" width="280" height="360" alt="Free eBook">
						<br><br>
						<a href="/e-book-download/" title="Download FREE eBook" class="btn btn-style1 btn-download">
							<em>Download</em>"FREE eBook"
						</a>
						<br>
						<small>
							<em>(Registration Required)</em>
						</small>
					</div>
				</div>
			</aside>

		</section>
	</div>		
		<!--A horizontal rule to show a distinct gap between the blogs-->
			
	



		<hr>
								
		<div class="section-row-container row-highlight">
		<!-- Page Block Two -->						   
			<?php get_template_part('templates/main', 'blocks-two') ?>											   
		<!-- Page Block Two END -->	
		</div>
		
		<hr>		
		
		<div class="section-row-container">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
						<p>If any of these programmes interest you and your child then why not book a trial session with us today.</p>			
						<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Book a trial session today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
				</article>							
			</section>			
		</div>
											
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
</div><!-- Main Section END -->
		
<hr>					   

<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts') ?>
  










