
<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Events' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID;
	$content = $post_obj->post_content;
?>

<body id="pageEVENTS" class="two-column-layout blog-layout">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
				<?php get_template_part('templates/cta', 'btn'); ?>
				<?php get_template_part('templates/social', 'links'); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section section id="tcsa-section1" class="w960 clearfix blogposts">				
					
					<article class="col-1">

						<article id="blogpost1" class="contentbox">
							<header>
								<h3>The August/September 2012 Events List</h3>
								<h4 class="datestamp"><span>Posted:</span> August 17th 2012</h4>
								<figure class="blogimage">
									<span class="imgstacked"><img src="../assets/img/TCSA-Aug-Sept-Events-small.png" width="600" height="839" alt="The August/September 2012 Events List"/>
									<span class="imgstack l1"></span><span class="imgstack l2"></span></span>									
									<figcaption><? echo $compname; ?> The August/September 2012 Events List</figcaption>
								</figure>						
								<p>Set your child on the road to becoming a ‘Winner in Soccer And In Life’ with some fantastic events and programmes throughout August and September.</p>
							</header>
							<footer>
								<p class="btn-container"><a href="javascript:history.back()" title="back to Blog" class="btn">Back</a> <a href="<?php echo $content ?>" title="Download Events PDF" class="btn btn-style1" target="_blank">Download Events PDF</a></p>
							
								<div class="shareplate clearfix">
									<h4>Share this page</h4>						
									<div class="g-plusone" data-size="medium" data-annotation="none" data-href="http://<? echo $site_url; ?>"></div>
									<script type="text/javascript">
									  window.___gcfg = {lang: 'en-GB'};
									
									  (function() {
										var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
										po.src = 'https://apis.google.com/js/plusone.js';
										var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
									  })();
									</script>
									<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<? echo $site_url; ?>" data-count="none">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
									<div class="fb-like" data-href="http://<? echo $site_url; ?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
								</div>
								
							</footer>
						</article>
						
						<article id="fbcomment-block" class="contentbox clearfix ">			
							<h4>Comments</h4>
							<div class="fb-comments" data-href="http://www.thechampionssocceracademy.com" data-num-posts="10" data-width="600"></div>													
						</article>
					
					</article>
					
					<aside class="sidebar col-2">
						<div class="sideblock item1">
							<?php get_sidebar(); ?>
						</div>
						<div class="sideblock item2 no-border">
							<div class="product-adblock">
								<img src="/assets/img/ebook-consumerguide.png" width="280" height="360" alt="Free eBook">
								<br><br>
								<a href="/e-book-download/" title="Download FREE eBook" class="btn btn-style1 btn-download">
									<em>Download</em>"FREE eBook"
								</a>
								<br>
								<small>
									<em>(Registration Required)</em>
								</small>
							</div>
						</div>
					</aside>
										
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   

</body>



<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  