<?php get_template_part('templates/head'); ?>

<body id="page404" class="default-layout">


	<div class="WRAPPER">


<?php get_template_part('templates/page', 'header'); ?>

<section id="BANNER" class="section-container banner-default">
	
				<article id="page-banner">
				
					<div class="banner-patternfx topstrip"></div>			
					<div class="banner-patternfx bottomstrip"></div>	
						
				</article>
					
			</section>	



<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">Sorry Page Not Found</h2>
				<a href="/contact.php" title="Get In Touch" class="btn btn-style1 cta2"><span>Get In Touch</span></a>
				<?php get_template_part('templates/social', 'links'); ?>				
			</section>	
		</div>





		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					
					<ul class="linklist">
						
						<?php wp_list_pages('&title_li=&child_of=23&link_before=<span>&link_after=</span>&depth=1'); ?>
					
					</ul>

					<div class="contentbox section-intro">
						<p>Oops!, Sorry but the page you are looking for is no longer available or has been moved to another section of this site.<br><br>
						<a href="/" title="Home" class="btn btn-style1"><? echo "Return to the Homepage"; ?></a><br><br></p>						
					</div>					
				</article>					
			</section>			
		</div>

		<!--<?php get_search_form(); ?>-->
	</div>
</body>
	<script type="text/javascript" src="assets/js/libs/jquery.easing.js"></script>
    <script type="text/javascript" src="assets/js/libs/jquery.h5validate.js"></script>
    <script src="assets/js/plugins-TCSA.js"></script>
    <script src="assets/js/script.js"></script>
