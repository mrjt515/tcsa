<?php get_template_part('templates/head'); ?>

<body class="two-column-layout">

<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Programmes' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
?>	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
				<?php get_template_part('templates/cta', 'btn'); ?>	
				<?php get_template_part('templates/social', 'links'); ?>		
			</section>	
		</div>
		
		<!-- Main Content Section -->
<?php get_template_part('templates/template', 'programmes'); ?>

<!--
<?php 
/* 
	Version 1
	Works quite nicely but there needs to be a lot of formatting for it to look like it should
*/
?>

<?php 
	$pages=get_pages(array('child_of'=>23));


	for ($i=0; $i < count($pages); $i++) { 
			$post = '<div class="row-fluid"><div class="span5 offset3">%1$s</div><div class="span3 offset1">%2$s</div></div>';
		
		if($i%2 !=0)
			$post = '<div class="row-fluid"><div class="span3 offset1">%2$s</div><div class="span5 offset3">%1$s</div></div>';

		$title = $pages[$i]->post_title;
		$img   = '<img src="//placehold.it/200x200" height="200" width="200" />';

		printf($post, $title, $img);
	}
?>-->


<?php /* 
	Version 2
	Works like a charm
*/ ?>
<style>

.a { 
	width:60%; 
} 

.b { 
	width:32%;
	padding-left: 20px;
	padding-top: 25px;
} 

.c {
	width:32%;
	padding-right: 30px;
	padding-top: 25px;
}

.a, .b, .c { 
	float: left;
}

</style>

<?php 
	$programmesPages=get_pages(array('child_of'=>23));
	

	for ($i=0; $i < count($programmesPages); $i++) { 
		$link = $programmesPages[$i]->post_name;

			$sec_f = 	'<div class="section-row-container row-highlight">
							<section id="tcsa-section2" class="w960">
								<article class="contentbox clearfix">
								%1$s
								%2$s
								</article>
							</section>
						</div>';

			$head_f = 	'<header class="a">
							<h3>%s</h3>
							<h4>%s</h4>
							%s...
							<br>
							<p><a href="/programmes/'.$link.'/" title="Learn More" class="btn">Learn More</a></p>
						</header>';

			$img_f = 	'<figure class="b">
							<span class="imgstacked">
								<img src="%s" alt="%s" width="%d" height="%d" />
								<span class="imgstack l1"></span><span class="imgstack l2"></span>
							</span>
						</figure>';
		
		if($i%2 !=0) {
			$img_f = 	'<figure class="c">
							<span class="imgstacked">
								<img src="%s" alt="%s" width="%d" height="%d" />
								<span class="imgstack l1"></span><span class="imgstack l2"></span>
							</span>
						</figure>';

			$sec_f = 	'<div class="section-row-container">
							<section id="tcsa-section2" class="w960">
								<article class="contentbox clearfix">
								%2$s
								%1$s
								</article>
							</section>
						</div>
						<hr>';

			
		}

		$img = $img_alt = $img_width= $img_height  = 0;

		if (has_post_thumbnail($programmesPages[$i]->ID)):
			$image = wp_get_attachment_image_src(get_post_thumbnail_id($programmesPages[$i]->ID), 'medium');
			$imgAlt = $programmesPages[$i]->post_title;
			$width = $image[1];
			$height = $image[2];
		endif;

		$post_title = $programmesPages[$i]->post_title;
		$sub_title  = 'Check out this Programme!';
		$description = $programmesPages[$i]->post_content;
		$description = substr($description, 0, strpos($description, ' ', 150));


		$img=$image[0];
		$imgPath = $programmesPages[$i]->post_name;
		$imgSource = '/assets/img/programmes/'.$imgPath.'.jpg';
		$imgAlt = $programmesPages[$i]->post_title;
		$width = $image[1];
		$height = $image[2];

		$divClass = 'row-highlight';

		$head = sprintf($head_f, $post_title, $sub_title, $description);
		$img = sprintf($img_f, $img, $imgAlt, $width, $height);

		$sec = sprintf($sec_f, $head, $img, $divClass);

		printf($sec, $head, $img);
		
	}
	
?>

		
		<hr>
		
		<hr>		
		
		<div class="section-row-container">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
					<p>If any of these programmes interest you and your child then why not fill out a pre-qualify form with us today.</p>			
					<p class="btn-container"><a href="<? echo $page_url3; ?>" title="Fill out a Pre-qualify form today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
					</article>							
			</section>			
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	  	



<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts')?>
  
</body>
</html>