<?php get_template_part('templates/head'); ?>

<body id="page404" class="default-layout fullwidth-layout">


  
<div id="WRAPPER">
		
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">About Us</h2>
				<?php get_template_part('templates/cta', 'btn'); ?>	
				<?php get_template_part('templates/social', 'links'); ?>		
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<li><a href="#sponsors" title="Our Sponsors">Our Sponsors</a></li>
						<li><a href="#philosophy" title="Our Philosophy">Our Philosophy</a></li>
						<li><a href="#coachingstandards" title="Coaching Standards">Coaching Standards</a></li>
						<li><a href="#trainingfacilities" title="Our Facilities">Our Facilities</a></li>
						<li><a href="#NLPtechniques" title="NLP Techniques">NLP Techniques</a></li>
					</ul>					
					<div class="contentbox section-intro">
						<p>We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs. <?php /*?><a href="#spilltxt1" title="Read More" class="btn btnstyle-inline overspill-trigger">Read More</a><?php */?></p>
						<?php /*?><div id="spilltxt1" class="text-overspill hide">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. </p>
							<p>Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
						</div><?php */?>
					</div>					
				</article>					
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container row-highlight">			
			<section id="tcsa-section2" class="w960 ltr">			
				<article id="sponsors" class="contentbox clearfix">			
					<header>
						<h3>Our Sponsors</h3>
						<figure class="right">
							<img src="/assets/img/logo-sponsor-lrg.png" width="280" height="60" alt="<? echo $sponsor_name; ?> Logo"/>
							<figcaption>The Champion's Soccer Academy sponsored by Rapidgrid</figcaption>
						</figure>
						<p>The Champion’s Soccer Academy is proud to be supported by Rapidgrid who are our main sponsor. We welcome the opportunity to work with business partnerships that share our vision and who want to be a part of an exciting project.</p>
					</header>
					<footer>
						<p class="btn-container"><a href="mailto:corporate@thechampionssocceracademy.com" title="Business Opportunities" class="btn">Business Opportunities</a> <a href="http://www.rapidgrid.co.uk" title="Visit the Rapidgrid website" class="txtlink" target="_blank">Visit the Rapidgrid website</a></p>
					</footer>								
				</article>							
			</section>						
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section3" class="w960 ltr">			
				<article id="philosophy" class="contentbox clearfix">			
					<header>
						<h3>Our Philosophy</h3>
						<figure class="right">
							<span class="imgstacked">
								<img src="/assets/img/philosophy.jpg" width="340" height="210" alt="<? echo $compname; ?>"/>
								<span class="imgstack l1"></span><span class="imgstack l2"></span>
							</span>
							<figcaption>The Champion's Soccer Academy</figcaption>
						</figure>
						<p>Our service includes the essential but often overlooked aspects of player development: Learning skills, performance skills, nutrition and lifestyle coaching (for players and parents), communication and social skills and parental support – helping parents to communicate and relate to their children in a way that empowers their football and life development. These elements are both integrated into our regular ‘football-specific’ live sessions and delivered as stand-alone programmes. <?php /*?><a href="#spilltxt1" title="Read More" class="btn btnstyle-inline overspill-trigger">Read More</a><?php */?></p>
						<?php /*?><div id="spilltxt1" class="text-overspill hide">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. </p>
							<p>Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
						</div><?php */?>
						
					</header>								
				</article>							
			</section>						
		</div>
		
		<hr>		
		
		<div class="section-row-container row-highlight last">
			<!-- Page Block Two -->						   
			<?php get_template_part('templates/main', 'blocks-two'); ?>												   
			<!-- Page Block Two END -->	
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  
	
	  	
</div>


<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  
</body>
</html>