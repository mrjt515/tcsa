<?php
		$page = explode("/", $_SERVER['REQUEST_URI']);
		$page = $page[count($page) - 1];
		switch($page) {
				case "":
				case "index.php":
		?>	
		<!--Creating an array which stores the 5 latest blog posts from all categories-->
		<?php
		$posts = get_posts(array('category' => '1,3,4,5,6,7,8,9,10,11'));
		?>



	<section id="BANNER" class="section-container">
	
				<article id="home-banner">
				
					<div class="banner-patternfx topstrip"></div>
					
					<div class="banner-message-block w960">
						<div class="overlay-screen clearfix">
							<hgroup class="banner-text">
								<h1>Winners in Soccer and Life</h1>
								<h2>Breaking new ground with NLP and other powerful tools in all of our coaching programmes.<a href="/e-book-download/" title="Download" class="btn btn-style1 cta1"><span>Download TCSA</span>FREE eBook</a></h2>
							</hgroup>
														
						</div>
					</div>
					
					<hr />
					

					


					<div class="banner-patternfx bottomstrip">
						<div id="newsbar" class="w960 clearfix">
							<div class="icon-cal left"></div>


							<ol class="newsticker left">
								<!--This is the loop which is actually running through the array-->
								<?php

								foreach ($posts as $key => $value) {
									?>
									<li class="post"><a href="<?=get_permalink($value->ID)?>"><?=$value->post_title?></a></li>
									<?php
								}
								?>

								<!--
								<li class="post"><a href="/blog/" title="Read more">'Pathway to Success' 18th &amp; 19th August 2012 the official TCSA launch Event.</a></li>
								<li class="post"><a href="/news2.php" title="Read more">Get ready for the start of the new football season Saturday 18th August 2012.</a></li>
								<li class="post"><a href="/news3.php" title="Read more">The Champion's Soccer Academy at the Welsh Super Cup July 23rd 2012.</a></li>

							-->
							</ol>		
							<?php get_template_part('templates/social', 'links'); ?>		
						</div>
					</div>	
						
				</article>
					
			</section>
		
		<?php
				break;			
				default :
		?>
<?php }?>



