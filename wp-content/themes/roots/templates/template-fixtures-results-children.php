<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
	$post_slug = $post_obj->post_name;
	$parent_ID = $post_obj->post_parent;
	$post_content = $post_obj->post_content; 
	$post_content = apply_filters( 'the_content', $post_content);
	$post_content = str_replace(']]>', ']]&gt', $post_content);
?>

<div class="section-row-container pagetitle-head">
	<section id="tcsa-section0" class="w960 clearfix">				
		<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
		<?php get_template_part('templates/cta', 'btn'); ?>	
		<?php get_template_part('templates/social', 'links'); ?>		
	</section>	
</div>

<div class="section-row-container row-highlight">			
	<section id="tcsa-section2" class="w960 ltr">			
		<article id="prog1" class="contentbox clearfix">
			<br>			
			<header class="a">

						<?php echo $post_content; ?>

			</header>										
		</article>							
	</section>						
</div>
