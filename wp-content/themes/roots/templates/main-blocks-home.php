<section id="home-blocks" class="w960 col3 clearfix">
	
	<article class="block item1">
		<a href="/free-resources/" title="<? echo $page_title1; ?>"><img src="../../assets/img/soccer-resources.jpg" width="300" height="135" alt="<? echo $page_title1; ?>" /></a>
		<h2><span><em>Free</em> Resources</span></h2>
		<p>Download a free ebook explaining our philosophy and what you can expect from The Champion’s Soccer Academy.</p>
		<p class="btn-container"><a href="/free-resources/" class="btn btn-style1" title="<? echo $page_title1; ?>">FREE RESOURCES</a></p>	
	</article>
	
	<article class="block item2">
		<a href="/programmes/" title="<? echo $page_title2; ?>"><img src="../../assets/img/soccer-courses.jpg" width="300" height="135" alt="<? echo $page_title2; ?>" /></a>
		<h2><span><em>Our</em> Courses</span></h2>
		<p>Our coaching curriculum goes way beyond the mandatory standards expected of junior and youth coaches.</p>
		<p class="btn-container"><a href="/programmes/" class="btn" title="<? echo $page_title2; ?>">View our programmes</a></p>
	</article>
	
	<article class="block item3">
		<a href="/events/" accesskey=""title="Take a look at our events"><img src="../../assets/img/soccer-matchday.jpg" width="300" height="135" alt="<? echo $events_linktitle; ?>" /></a>
		<h2><span>Match Day Events</span></h2>
		<p>Catch up with the latest news and events about match days including team fixtures and training schedules.</p>
		<p class="btn-container"><a href="/events/" class="btn" title="Take a look at our events">Read More</a></p>
	</article>

</section>