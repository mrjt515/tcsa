<footer id="page-footer">
  	
	<section id="foot-sect1" class="section-container">
		<div class="w960">		
			<?php get_template_part('templates/form', 'newsletter'); ?>					
		</div>
	</section>
	
	<hr>

	<section id="foot-sect2" class="section-container">		
		<div id="logo-strip" class="w960 clearfix">			
			<ul class="accreditation-logos left">
				<li class="first"><img src="/assets/img/logo-tcsa-badge.png" width="60" height="65" alt="Under 13's badge" /></li>
				<li><img src="/assets/img/logo-tcsa-lchampsbadge.png" width="60" height="65" alt="Under 10's badge 'Little Champs'" /></li>
				<li class="last sponsor"><a href="http://www.rapidgrid.co.uk" title="Sponsored by Rapidgrid" target="_blank"><span class="smll hex2">Sponsored by<br /><? echo $sponsor_name; ?><br /></span><img src="/assets/img/logo-sponsor-med.png" width="110" height="65" alt="<? echo $sponsor_name; ?>" /></a></li>
			</ul>			
			<div id="footer-social-block" class="right">
				<h5>Follow us on</h5>
				<?php get_template_part('templates/social', 'links') ?>
			</div>					
		</div>
	</section>
	
	<hr>
	
	<section id="foot-sect3" class="section-container">	
		<div class="w960">	
			<nav id="footer-menu">
				<ul class="clearfix">
				<?php
			          if (has_nav_menu('primary_navigation')) :
			            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav'));
			          endif;
			        ?>
				</ul>
			</nav>
		
			<hr>
	
			<div id="disclaimer-strip">						
				<ul class="disclaimer-links clearfix">
					<li class="first"><span class="copyright">Copyright &copy;<?php ini_set('date.timezone', 'Europe/London'); echo date('Y'); ?></span> <span class="inline"><? bloginfo('name') ?>, <br/>32 Wern Road, Swansea, United Kingdom, SA1 2PA</span><br/><br/></li>			
					<li><span class="icon-tel">Tel.</span> <span>01792 421 600</span></li>
					<li><span class="icon-email">Email.</span> <a href="mailto:info@thechampionssocceracademy.com" title="Send us an Email">info@thechampionssocceracademy.com</a><br/></li>
					<li class="newline"><small>Registered in England and Wales, Reg No. 8036846</small></li>
					<li class="footer-logo"><img src="/assets/img/logo-mainfooter.png" width="66" height="70" alt="<? echo $compname; ?> Logo" /></li>
					<li class="last"><a href="privacy-policy.php" title="Our Privacy Policy">Privacy Policy</a></li>
				</ul>			
			</div>			
		</div>	
	</section>
	
	<hr>
	
	<section id="foot-sect4" class="section-container">
		<div id="designer-idblock" class="w960">
			<img src="/assets/img/HTML5_Badge_32.png" alt="HTML5 Badge" width="32" height="32" class="html5-badge"/><br/>Web Design by <a href="http://www.brightseed.co.uk" title="Web Site Design Brightseed" target="_blank" class="brightseed-ident">Brightseed</a>
		</div>
	</section>	


</footer>

<a href="#" title="Back to Top" class="toTop btn btn-style1"><span>Top</span></a>