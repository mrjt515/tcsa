<div class="section-row-container">		
	<section id="tcsa-section1" class="w960 page-intro">				
		<article class="clearfix">					
			<header>
				<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
			</header>					
					
				<ul class="linklist">
					<?php wp_list_pages('&title_li=&child_of=23&link_before=<span>&link_after=</span>&depth=1'); ?>
				</ul>
										
			<div class="contentbox section-intro">
				<p>We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs. Your child will benefit from a combined 100 + years of coaching experience from grass-roots to elite level.</p>
			</div>					
		</article>					
	</section>			
</div>