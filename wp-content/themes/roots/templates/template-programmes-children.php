<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
	$post_slug = $post_obj->post_name;
	$parent_ID = $post_obj->post_parent;
	$content = $post_obj->post_content;
?>

	<div class="section-row-container pagetitle-head">
		<section id="tcsa-section0" class="w960 clearfix">				
			<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
			<?php get_template_part('templates/cta', 'btn'); ?>	
			<?php get_template_part('templates/social', 'links'); ?>		
		</section>	
	</div>
	
	
	
	<div class="section-row-container row-highlight">			
			<section id="tcsa-section2" class="w960 ltr">			
				<article id="prog1" class="contentbox clearfix">			
					<header class="sideA">

						<?php echo $content ?>
						<br>
						<br>
						<h2>Blogs about the Programme</h2>
						

						<? query_posts('category_name=' . $post_slug); ?>
						<?php while (have_posts()) : the_post(); ?>
							<li>	
								<h4>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h4>
							</li>

						<?php endwhile; ?>

					</header>
					<figure class="sideB">
						<?php echo '<span class="imgstacked"><img src="../../assets/img/programmes/'.$post_slug.'" width="340" height="210" alt="<?php echo '.$post_slug.' ?>"/>' ?>
						<span class="imgstack l1"></span><span class="imgstack l2"></span></span>
						<figcaption class="hide"><? echo $compname; ?> <? echo $service_menu_1; ?></figcaption>
					</figure>													
				</article>							
			</section>						
		</div>




