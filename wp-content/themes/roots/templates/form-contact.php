<form action="process.php" method="post" autocomplete="on" id="contactform" class="pageform clearfix">
	<h3 class="form-title">Send us an Enquiry</h3>
	<fieldset><legend>Your Details</legend>
		<div>
		  <span class="label"><label for="cust-fullname">Name:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. John Smith" name="cust-fullname" id="cust-fullname" class="txtbox required"  required="required" /></span>
		</div>
		
		<div>
		  <span class="label"><label for="cust-contact">Contact Tel:</label></span>
		  <span class="data"><input type="text" name="cust-contact" id="cust-contact" class="txtbox telno" placeholder="e.g. 01792 145 876" /></span>
		</div>
		
		<div>
			<span class="label"><label for="cust-email">Email:</label></span>
			<span class="data"><input type="email" placeholder="e.g. johnsmith@myemail.com" name="cust-email" id="cust-email" class="txtbox emailadd required h5-email" required="required" /><br />
<small class="fieldinstruction">Email is required for us to keep you informed with programme updates</small></span>
		</div>
		
		<div>
		  <span class="label"><label for="cust-addrress">Address:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. 21 Edward Close, Bute Town, Cardiff" name="cust-addrress" id="cust-addrress" class="txtbox address" /></span>
		</div>
		
		<div>
		  <span class="label"><label for="cust-pcode">Post Code:</label></span>
		  <span class="data"><input type="text" name="cust-pcode" id="cust-pcode" class="txtbox postcode" placeholder="e.g. CF10 8UT"  /></span>
		</div>
	</fieldset>		
	
	<fieldset><legend>Additional Information</legend>
		<div>
		  <span class="label"><label for="cust-interests">I'm Interested In:</label></span>
		  <span class="data"><select class="b-core-ui-select__select" name="cust-interests" id="cust-interests">
			<option value="" disabled="disabled" selected="selected">- Please Select -</option>
			<option value="Elite Player Development">Elite Player Development</option>
			<option value="Skills Accelerator">Skills Accelerator</option>
			<option value="Confident Player">Confident Player</option>
			<option value="Winning Attitude">Winning Attitude</option>
			<option value="Pro’ Preparation">Pro’ Preparation</option>
			<option value="Parent Training">Parent Training</option>
			<option value="Nutrition &amp; Lifestyle">Nutrition &amp; Lifestyle</option>
			</select></span>
		</div>
		<div>
		  <span class="label">Comments</span>
		  <span class="data"><textarea placeholder="Enter additional comments here:" name="custcomments" id="custcomments" class="txtbox" ></textarea></span>
		</div>
		<div>
		  <span class="label">Newsletter</span>
		  <span class="data multiple"><input type="checkbox" name="cm-ol-jdvn" id="cmWebsiteNewsletterRegistrations" checked="checked" /> <label for="cmWebsiteNewsletterRegistrations">I would like to subscribe to the newsletter and updates</label></span>
		</div>
	</fieldset>
	
	<fieldset class="last">
		<div>
			<span class="spacer">&nbsp;</span>
			<span class="btn-container"><input type="submit" value="Send Enquiry" class="btn submitbtn" /></span>
		</div>
	</fieldset>
	   
</form> 

<div id="formstep2" class="success hide">
	<h3 class="form-title success">Thank you for your Enquiry</h3>
	<p>Your enquiry has been sent and we will get back to you as soon as we can.</p>
</div>