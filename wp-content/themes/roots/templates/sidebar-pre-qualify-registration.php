<aside>
	<div class="sideblock item1">
		<h4 class="title t-faq">
			<a href="/faqs/">FAQs</a>
		<br>
		<span>Your questions answered</span>
		</h4>
		<ul class="linklist">
			<li>
				<a href="/faqs/#q1">Is my child too young?</a>
			</li>
			<li>
				<a href="/faqs/#q2">Do you have a programme for girls?</a>
			</li>
			<li>
				<a href="/faqs/#q3">What programme is best suited to my child's ability?</a>
			</li>
			<li>
				<a href="/faqs/#q4">How can parents get involved?</a>
			</li>
		</ul>
	</div>	
	<div class="sideblock item2">
		<h4 class="title t-contact">
			Get in Touch
			<br>
			<span>Speak to one of our Coaches</span>
		</h4>
		<div class="contact-wrap">
			<p>
			<span class="smalltxt">Call us on:</span>
				<strong>01792 421 600</strong>
			</p>
			<h5>
				<em>Mon - Fri:</em>
					9am - 5pm
				<em>Sat:</em>
					10am - 2pm
			</h5>
			<a href="mailto:info@thechampionssocceracademy.com?Subject=I would like more information about TCSA" title="Send us an Email" class="btn btn-email">
				<span>Send us an Email</span>
			</a>
		</div>
	</div>
					
	<div class="sideblock item3">
		<ul class="coach-idtag clearfix">
			<li class="coach-avatar">
				<img src="/assets/img/avatar-frankie-burrows.png" width="100" height="100" alt="Frankie Burrows">
			</li>
			<li class="coach-label">
				<span class="staff-title">Head Coach</span>
				<br>
				<span class="staff-name">Frankie Burrows</span>
				<br>
				<a href="mailto:frankie@thechampionssocceracademy.com?Subject=Message:" title="Send a Message to Frankie Burrows" class="btn btn-style1 btnmessage">
					Send a Message
				</a>
			</li>
		</ul>
	</div>
</aside>

