<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="mainsection" role="main">

  <?php /*
  The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
  button) so should the name of the page be changed from 'Blog' then the title at the top will also change.
*/  
  $post_obj = $wp_query->get_queried_object();
  $post_ID = $post_obj->ID; 
?>

<style type="text/css">
  h1 {
    font-family: 'highlandgothiclightflfregular', Arial, Helvetica, sans-serif;
    font-size: 30px;
  }

  .blogheader {
    text-align: right;
    font-family: 'highlandgothiclightflfregular', Arial, Helvetica, sans-serif;
  }
  
</style>

  <div class="section-row-container pagetitle-head">
    <section id="tcsa-section0" class="w960 clearfix">        
      <h2 class="page-title">Blog</h2>
      <?php get_template_part('templates/cta', 'btn'); ?> 
      <?php get_template_part('templates/social', 'links'); ?>    
    </section>  
  </div>


  <div class="section-row-container">
    <section id="tcsa-section1" class="w960 clearfix blogposts">
      <article class="col-1">
        <?php while (have_posts()) : the_post(); ?>

          <article <?php post_class(); ?>>
            
              

              <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <div class="blogheader">
                  <?php get_template_part('templates/entry-meta'); ?>
                </div>
              </header>
              <br>
              <div class="entry-content">
                <?php the_content(); ?>
              </div>
            
              <footer>
                  
                <!--<?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>-->
                
              </footer>
              <!-- CHANGE -->
            
            <article id="fbcomment-block" class="contentbox clearfix ">
              <div class="fb-comments" data-href="http://www.thechampionssocceracademy.com" data-num-posts="10" data-width="600"></div>                         
            </article>

          </article>
        <?php endwhile; ?>
      </article>

      <aside class="sidebar col-2">
        <div class="sideblock item1">
          <?php get_sidebar('sidebar', 'primary'); ?>
        </div>
        <div class="sideblock item2 no-border">
          <div class="product-adblock">
            <img src="/assets/img/ebook-consumerguide.png" width="280" height="360" alt="Free eBook">
            <br><br>
            <a href="/e-book-download/" title="Download FREE eBook" class="btn btn-style1 btn-download">
              <em>Download</em>"FREE eBook"
            </a>
            <br>
            <small>
              <em>(Registration Required)</em>
            </small>
          </div>
        </div>
      </aside>

    </section>
  </div>



</div>