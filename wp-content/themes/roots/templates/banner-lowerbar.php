<?php
		$posts = get_posts(array('category' => '1,3,4,5,6,7,8,9,10,11'));
?>
<div class="banner-patternfx bottomstrip">
	<div id="newsbar" class="w960 clearfix">
		<div class="icon-cal left"></div>
			<ol class="newsticker left">
				<!--This is the loop which is actually running through the array-->
					<?php
						foreach ($posts as $key => $value) {
					?>
						<li class="post"><a href="<?=get_permalink($value->ID)?>"><?=$value->post_title?></a></li>
					<?php } ?>
			</ol>		
				<?php get_template_part('templates/social', 'links'); ?>		
	</div>
</div>