<?php
  
  	//********************************************************
	// PUBLISHER INFORMATION
	//********************************************************
  	$designer = "Brightseed Creative Agency";
	$publisher = "Brightseed Creative Agency";
  
	
	
	//********************************************************
	// SEO INFORMATION
	//********************************************************  	 
	 
	$seotitle = "South Wales Junior and Youth Soccer Academy"; 
	$seokeywords = "South Wales, Junior and Youth Soccer Academy"; 
	$seosummary = "We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.";	
	$seoabstract = ""; 
	 
  
  
  
	//********************************************************
	// CONTACT DETAILS
	//********************************************************   
  
	// General information
	$compname = "The Champion's Soccer Academy";
  	$site_url = "www.thechampionssocceracademy.com";  
  
	// Company Contact Details	
	$location_name = "Main Office";
	$comptel = "01792 421 600";
	$compfax = "?";
	$address = "32 Wern Road, <br/>Swansea, <br/>United Kingdom, <br/>SA1 2PA";
	$maplink = "http://goo.gl/maps/lt1LQ";
	$weekhrs = "<em>Mon - Fri:</em> 9am - 5pm";
	$weekendhrs = "<em>Sat:</em> 10am - 2pm";
	
		
	// Company Registraion Details
	$compregno = "8036846";
	$compvat = "?";	
	
	// Email Addresses
	$compemail = "info@thechampionssocceracademy.com";
	$compemail2 = "frankie@thechampionssocceracademy.com";
	$compemail3 = "bookings@thechampionssocceracademy.com";
	$compemail4 = "corporate@thechampionssocceracademy.com";
	$compemail5 = "support@thechampionssocceracademy.com";
	
	// Contact form email	
	$enquiryemail = "frankie@thechampionssocceracademy.com";
	$sendemail = "frankie@thechampionssocceracademy.com";
	$sendemail2 = "";
	$sendemail3 = "";
	$sendemail4 = "";

	// Sandbox testing
	$testemail = "";
	
	
	
	//********************************************************
	// COACHES
	//********************************************************
	
	// Team Members
	$coachtitle_1 = "Head Coach";
	$coachname_1 = "Frankie Burrows";
	$coachtwitter_1 = "CoachFrankieB";
	$coachemail_1 = "frankie@thechampionssocceracademy.com";
	
	// Sponsor Name
	$sponsor_name = "Rapidgrid";
	$sponsor_url = "http://www.rapidgrid.co.uk";
	
	
		
		
	//********************************************************
	// TOP LEVEL PAGE URLS
	//******************************************************** 	 	
	
	// Home Page	
	$home_url = "/";
	$nav_home= "Home";
	$home_title= "Return to the home page";	
	
	// Menu options Pages	
	$page_url1 = "/resources.php";				
	$nav_menu1= "FREE Resources";					
	$page_title1= "Free Resources for you to download";
	
	$page_url2 = "/programmes.php";				
	$nav_menu2= "Programmes";					
	$page_title2= "TCSA Programmes";
	
	$page_url3 = "/booking.php";				
	$nav_menu3= "Pre-qualify Registration";					
	$page_title3= "Pre-qualify Registration";
	
	$page_url4 = "/ebook-download.php";				
	$nav_menu4= "FREE Downloads";					
	$page_title4= "FREE Soccer Skills Guide eBook";
	
	
	// Service Menu Pages
	$service_menu_url1 = "programmes.php#prog1";	
	$service_menu_1 = "Elite Player Development";
	$service_menu_title1 = "Elite Player Development";
	
	$service_menu_url2 = "programmes.php#prog2";	
	$service_menu_2 = "Skills Accelerator";
	$service_menu_title2 = "Skills Accelerator";
	
	$service_menu_url3 = "programmes.php#prog3";	
	$service_menu_3 = "Confident Player";
	$service_menu_title3 = "Confident Player";
	
	$service_menu_url4 = "programmes.php#prog4";	
	$service_menu_4 = "Winning Attitude";
	$service_menu_title4 = "Winning Attitude";
	
	$service_menu_url5 = "programmes.php#prog5";	
	$service_menu_5 = "Pro’ Preparation";
	$service_menu_title5 = "Pro’ Preparation";
	
	$service_menu_url6 = "programmes.php#prog6";	
	$service_menu_6 = "Parent Training";
	$service_menu_title6 = "Parent Training";
	
	$service_menu_url7 = "programmes.php#prog7";	
	$service_menu_7 = "Nutrition &amp; Lifestyle";
	$service_menu_title7 = "Nutrition &amp; Lifestyle";
			
	
	// Standard page types
	$about_url = "/about.php";				
	$about_menu= "About Us";					
	$about_title= "About Us";
	
	$testimonials_url = "/success-stories.php";				
	$testimonials_menutitle= "Success Stories";					
	$testimonials_linktitle= "Read our success stories";
	
	$newsletter_url = "/newsletter.php";				
	$newsletter_menutitle= "Newsletter";					
	$newsletter_linktitle= "Sign up for our Newsletter";
	
	$news_url = "/blog.php";				
	$news_menutitle= "Blog";					
	$news_linktitle= "Read what's going on at TCSA";
	
	$events_url = "/events.php";				
	$events_menutitle= "Events";					
	$events_linktitle= "TCSA latest events";
	
	$faqs_url = "/faqs.php";				
	$faqs_menutitle= "FAQs";					
	$faqs_linktitle= "FAQs, your questions answered";	
	
	$contact_url = "/contact.php";				
	$contact_menutitle= "Get in Touch";					
	$contact_linktitle= "Contact Us";
	
	$login_url = "/login.php";				
	$login_menutitle= "Members Login";					
	$login_linktitle= "Member's Login Area";
	
	
	//function to idetify page we are on
		
	$URL = $_SERVER['REQUEST_URI'];
	
	switch($URL) {
		
		case'/':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Home',
							'TITLE'	=> 'South Wales Junior and Youth Soccer Academy',
							'KEY'	=> 'junior and youth soccer academy, soccer training, youth development programmes, south wales, llanelli, swansea, newport, bridgend, cardiff, UK',
							'DESC'	=> 'We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.'
							);
			break;
			
		case'/resources.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Resources',
							'TITLE'	=> 'FREE soccer skills and training resources',
							'KEY'	=> 'soccer skills and training resources',
							'DESC'	=> 'Download FREE soccer skills and training resources from The Champion/s Soccer Academy.'
							);
			break;
			
		case'/programmes.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Development Programmes',
							'TITLE'	=> 'Soccer Training Development Programmes',
							'KEY'	=> 'soccer training, youth development programmes',
							'DESC'	=> 'Fixed line services with NO fixed term contracts and No minimum call charges.'
							);	
			break;
		case'/about.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Information',
							'TITLE'	=> 'All about TCSA',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/booking.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Book A Programme',
							'TITLE'	=> 'Book Your Soccer Skills Programme',
							'KEY'	=> '',
							'DESC'	=> ''
							);
			break;
		case'/ebook-download.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'FREE Downloads',
							'TITLE'	=> 'FREE Soccer Skills Guide eBook',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/success-stories.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Success Stories',
							'TITLE'	=> 'Making Champions in Life',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/faqs.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'FAQs',
							'TITLE'	=> 'Your Questions Answered',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/events.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Events',
							'TITLE'	=> 'Junior and Youth Soccer Events in South Wales',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/blog.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Blog',
							'TITLE'	=> 'latest Soccer Academy News',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/news1.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Official Launch',
							'TITLE'	=> 'The Champions Soccer Academy Official Launch 2012',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/news2.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'New Football Season',
							'TITLE'	=> 'Get Ready For the New Football Season',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/news3.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Welsh Super Cup',
							'TITLE'	=> 'At the Welsh Super Cup 23rd July 2012',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/contact.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Contact',
							'TITLE'	=> 'Book your soccer skills programme',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
		case'/login.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Login',
							'TITLE'	=> 'TCSA Members Area',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;
			
		case'/privacy.php':
			$pageData = array(
							'URL'	=> '/',
							'NAME' 	=> 'Privacy',
							'TITLE'	=> 'Privacy Policy',
							'KEY'	=> '',
							'DESC'	=> ''
							);	
			break;	
			
		case'':
				
			break;
	
	}
		
	
	
	
	//********************************************************
	// HOME PAGE BANNER
	//********************************************************
	
	// Banner dimensions
	$bannerwidth ="960";
	$bannerheight ="300";
	
	
	// Slide 1
	$bannertitle1 = "Winners in Soccer and Life!";
	$bannerintro1 = "Breaking new ground with  <strong>NLP</strong> and other powerful tools in all of our coaching programmes.";
	$bannertooltip1 = "Winners in Soccer and Life!";
	$bannerlink1 = "/programmes.php";
	
	// Slide 2
	$bannertitle2 = "<span class='line1'>Save up to <em class='hex2'>30%</em>...</span><br><strong>No</strong> Fixed term contracts";
	$bannerintro2 = "We <strong>guarantee to beat</strong> your existing Call and Rental tariffs by as much as <strong>30%</strong> so call us today and start saving. ";
	$bannertooltip2 = "Save up to 30% - No Fixed term contracts";
	$bannerlink2 = "/fixed-line-services.php";
	
	// Slide 3
	$bannertitle3 = "<span class='line1'>Add extra meaning to every word...</span><br> Talk<span class='hex2'>+</span>Donate";
	$bannerintro3 = "The telecoms company that <strong>gives to good causes</strong><br>as an integral part of its service offer.";
	$bannertooltip3 = "Add extra meaning to every word with Talk+Donate";
	$bannerlink3 = "/charitable-telecoms.php";
	
	// Slide 4
	$bannertitle4 = "<span class='line1'>Complex solutions made simple.</span><br>Telephone Systems";
	$bannerintro4 = "<strong>Future proof</strong> conventional or VOIP telephone systems<br>giving your business the flexibility to grow.";
	$bannertooltip4 = "Complex solutions made simple";
	$bannerlink4 = "/telephone-systems.php";	
	
	// Slide 5
	$bannertitle5 = "Banner 5";
	$bannerintro5 = "Blurb 5";
	$bannertooltip5 = "";
	$bannerlink5 = "#";
	
	// Slide 6
	$bannertitle6 = "Banner 6";
	$bannerintro6 = "Blurb 6";
	$bannertooltip6 = "";
	$bannerlink6 = "#";
	
	// Slide 7
	$bannertitle7 = "Banner 7";
	$bannerintro7 = "Blurb 7";
	$bannertooltip7 = "";
	$bannerlink7 = "#";
	
	
	
	//********************************************************
	// PROMOTIONAL MESSAGES
	//********************************************************
	
	$strapline = "Winners In Soccer And In Life";
		
	
			
	//********************************************************
	// SOCIAL NETWORK OPTIONS
	//********************************************************
	
	// TWITTER
	$twitter_feed = "";
	$twitter_count = "4";
	$twitter_url = "http://www.twitter.com/@CoachFrankieB";
	
	// FACEBOOK	
	$facebook_url = "http://www.facebook.com/TheChampionsSoccerAcademy";
	
	// GOOGLE+
	$gplus_url = "";
	
	// FLICKR
	$flickr_feed = "";
	
	// YOUTUBE
	$youtube_url = "";
	
	
	
	
	//********************************************************
	// ALERT MESSAGE PANEL
	//********************************************************
	
	$alert_message_title = "Information Alert";
	$alert_message = "Our email services are currently down, please contact us by phone";
	$alert_message_link = "/contact.php";
	
	
	//********************************************************
	// MISC
	//********************************************************
	
	$policydate = "August 16, 2012";
  
?>