<?php
		$page = explode("/", $_SERVER['REQUEST_URI']);
		$page = $page[count($page) - 1];
		switch($page) {
				case "contact.php":
		?>
		
<div class="sideblock mapbox item1">
	<h4 class="title t-map">Map<br /><span>Find where we are</span></h4>
	<a href="<? echo $maplink; ?>" title="View Map" target="_blank"><img src="/assets/img/tcsa-map.png" width="240" height="170" alt="Map" /></a>
	<div class="contact-wrap">
		<p><span class="smalltxt">Main Office:</span></p>
		<h5><span class="address-tag"><em><? echo $compname; ?></em><br /><? echo $address; ?></span></h5>
		<a href="<? echo $maplink; ?>" title="View Map" class="btn btn-style1">View Map</a>
	</div>
</div>

<div class="sideblock item2">								   
	<h4 class="title t-contact">Get In Touch<br /><span>Speak to one of our coaches</span></h4>
	
	<div class="contact-wrap">
		<p><span class="smalltxt">Call us on:</span> <strong><? echo $comptel; ?></strong></p>
		<h5><? echo $weekhrs; ?><br />
		<? echo $weekendhrs; ?></h5>
		<a href="mailto:<? echo $compemail; ?>?Subject=I would like more information about TCSA" title="Send us an Email" class="btn btn-email"><span>Send us an Email</span></a>
	</div>		
</div>

<div class="sideblock item3">								   
	<ul class="coach-idtag clearfix">
		<li class="coach-avatar"><img src="/assets/img/avatar-frankie-burrows.png" width="100" height="100" alt="<? echo $coachname_1; ?>" /></li>
		<li class="coach-label"><span class="staff-title"><? echo $coachtitle_1; ?></span><br /><span class="staff-name"><? echo $coachname_1; ?></span><br />
<a href="mailto:<? echo $coachemail_1; ?>?Subject=Message:" title="Send a Message to <? echo $coachname_1; ?>" class="btn btn-style1 btnmessage">Send a Message</a></li>
	</ul>		
</div>	

		<?php
				break;			
				case "blog.php":
				case "news1.php":
				case "news2.php":
				case "news3.php":
				case "events.php":			
		?>	
		
<div class="sideblock item1">
	<h4 class="title t-faq">Recent Posts<br /><span>Latest blog posts</span></h4>
	<ul class="linklist">
		<li><a href="news1.php" title="<? echo $compname; ?> Official Launch"><? echo $compname; ?> Official Launch</a></li>
		<li><a href="news2.php" title="Get Ready For the New Football Season">Get Ready For the New Football Season</a></li>
		<li><a href="news3.php" title="<? echo $compname; ?> at the Welsh Super Cup"><? echo $compname; ?> at the Welsh Super Cup</a></li>
	</ul>
</div>	

<div class="sideblock item2 no-border">
	<div class="product-adblock">
	<img src="/assets/img/ebook-consumerguide.png" width="280" height="360" alt="FREE eBook" /><br /><br />
	<a href="<? echo $page_url4; ?>" title="Download FREE eBook" class="btn btn-style1 btn-download"><em>Download</em> FREE eBook</a><br />
	<small><em>(Registration Required)</em></small>
	</div>
</div>
	
		<?php
				break;			
				case "booking.php":
		?>


<div class="sideblock item1">
	<h4 class="title t-faq">FAQs<br /><span>Your questions answered</span></h4>
	<ul class="linklist">
		<li><a href="/faqs.php#q1" title="Is my child too young?">Is my child too young?</a></li>
		<?php /*?><li><a href="/faqs.php#q2" title="How do I pay?">How do I pay?</a></li><?php */?>
		<li><a href="/faqs.php#q3" title="Do you have programmes for girls??">Do you have programmes for girls?</a></li>
		<li><a href="/faqs.php#q4" title="How do I know what programme is best suited to my child’s ability?">What programme is best suited to my child’s ability?</a></li>
		<li><a href="/faqs.php#q5" title="How can parents get involved?">How can parents get involved?</a></li>
		<?php /*?><li><a href="/faqs.php#q6" title="How long does a programme last?">How long does a programme last?</a></li>
		<li><a href="/faqs.php#q7" title="What happens when my child has completed a programme?">What happens when my child has completed a programme?</a></li>
		<li><a href="/faqs.php#q8" title="How can I cancel my membership?">How can I cancel my membership?</a></li><?php */?>
	</ul>
</div>

<hr />
	
<div class="sideblock item2">								   
	<h4 class="title t-contact">Get In Touch<br /><span>Speak to one of our coaches</span></h4>
	
	<div class="contact-wrap">
		<p><span class="smalltxt">Call us on:</span> <strong><? echo $comptel; ?></strong></p>
		<h5><? echo $weekhrs; ?><br />
		<? echo $weekendhrs; ?></h5>
		<a href="mailto:<? echo $compemail; ?>?Subject=I would like more information about TCSA" title="Send us an Email" class="btn btn-email"><span>Send us an Email</span></a>
	</div>		
</div>

<div class="sideblock item3">								   
	<ul class="coach-idtag clearfix">
		<li class="coach-avatar"><img src="/assets/img/avatar-frankie-burrows.png" width="100" height="100" alt="<? echo $coachname_1; ?>" /></li>
		<li class="coach-label"><span class="staff-title"><? echo $coachtitle_1; ?></span><br /><span class="staff-name"><? echo $coachname_1; ?></span><br />
<a href="mailto:<? echo $coachemail_1; ?>?Subject=Message:" title="Send a Message to <? echo $coachname_1; ?>" class="btn btn-style1 btnmessage">Send a Message</a></li>
	</ul>		
</div>

<hr />


<?php
				break;			
				default :
		?>

<?php }?>
	


