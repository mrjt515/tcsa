<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">					
					<header>
						<h2 class="page-caption">We are highly regarded in junior and youth football coaching, focused solely on developing your childs future.</h2>
					</header>					
					<ul class="linklist">
						<li><a href="#awarnessguide" title="Consumer Awarness Guide">Consumer Awarness Guide</a></li>
						<?php /*?><li><a href="#tutvideo1" title="Keep Up Video Tutorial">Keep Up Video Tutorial</a></li>
						<li><a href="#introtoNLP" title="An Introduction to NLP">An Introduction to NLP</a></li><?php */?>
					</ul>					
					<div class="contentbox section-intro">
						<p>We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.</p>
					</div>					
				</article>					
			</section>			
		</div>