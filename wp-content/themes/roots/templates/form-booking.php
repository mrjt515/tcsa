<form action="http://thechampionssocceracademy.createsend.com/t/j/s/jdduui/" autocomplete="on" method="post" id="subFormBooking" class="pageform clearfix">
						
	<fieldset><legend>Your Child’s Details</legend>
		<div>
		  <span class="label"><label for="ChildsAge">Age:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. 10" name="cm-f-xkyty" id="ChildsAge" class="txtbox agebox" width="3" /></span>
		</div>
		
		<div>
		  <span class="label"><label for="ChildsGender">Gender:</label></span>
		  <span class="data"><select class="b-core-ui-select__select" name="cm-fo-xkytj" id="ChildsGender">
		<option value="288263">Male</option>
		<option value="288264">Female</option>
		</select><br class="hide" />
<small class="fieldinstruction">These details are for putting your child into the correct programme</small></span>
		</div>
	</fieldset>
	
	<fieldset><legend>Your Details</legend>
		<div>
		  <span class="label"><label for="name-full">Name:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. John Smith" name="cm-name" id="name-full" class="txtbox required"  required="required" /></span>
		</div>
		
		<div>
		  <span class="label"><label for="Relationship">Relationship:</label></span>
		  <span class="data"><select class="b-core-ui-select__select" name="cm-fo-xkytk">
			<option value="288272">Parent</option>
			<option value="288273">Guardian</option>
			<option value="288274">Relative</option>
			</select></span>
		</div>
		
		<div>
		  <span class="label"><label for="ContactTel">Contact Tel:</label></span>
		  <span class="data"><input type="text" name="cm-f-xkytt" id="ContactTel" class="txtbox telno" placeholder="e.g. 01792 145 876" /></span>
		</div>
		
		<div>
			<span class="label"><label for="jdduui-jdduui">Email Address:</label></span>
			<span class="data"><input type="email" placeholder="e.g. johnsmith@myemail.com" name="cm-jdduui-jdduui" id="jdduui-jdduui" class="txtbox emailadd required" required="required" /><br />
<small class="fieldinstruction">Email is required for us to keep you informed with programme updates</small></span>
		</div>
		
		<div>
		  <span class="label"><label for="Address">Address:</label></span>
		  <span class="data"><input type="text" placeholder="e.g. 21 Edward Close, Bute Town, Cardiff" name="cm-f-xkytl" id="Address" class="txtbox address" /></span>
		</div>
		
		<div>
		  <span class="label"><label for="PostCode">Post Code:</label></span>
		  <span class="data"><input type="text" name="cm-f-xkytr" id="PostCode" class="txtbox postcode" placeholder="e.g. CF10 8UT"  /></span>
		</div>
	</fieldset>
	
	<fieldset><legend>Select A Programme</legend>
		<div>
			<span class="label"><span>Courses:</span></span>
			<span class="data multiple">
				<input type="checkbox" name="cm-fo-xkyth" id="cm288265" value="288265" /> <label for="cm288265">Elite Player Development</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288266" value="288266" /> <label for="cm288266">Skills Accelerator</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288267" value="288267" /> <label for="cm288267">Confident Player</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288268" value="288268" /> <label for="cm288268">Winning Attitude</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288269" value="288269" /> <label for="cm288269">Pro’ Preparation</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288270" value="288270" /> <label for="cm288270">Parent Training</label><br />
				<input type="checkbox" name="cm-fo-xkyth" id="cm288271" value="288271" /> <label for="cm288271">Nutrition &amp; Lifestyle</label>
			</span>
		</div>
	</fieldset>
	
	<fieldset><legend>Additional Information</legend>
		<div>
		  <span class="label">Comments</span>
		  <span class="data"><textarea placeholder="Enter additional comments here:" name="custcomments" id="custcomments" cols="30" rows="5" class="txtbox" ></textarea></span>
		</div>
		<div>
		  <span class="label">Newsletter</span>
		  <span class="data multiple"><input type="checkbox" name="cm-ol-jdvn" id="cmWebsiteNewsletterRegistrations" checked="checked" /> <label for="cmWebsiteNewsletterRegistrations">I would like to subscribe to the newsletter and updates</label></span>
		</div>
	</fieldset>
	
	<fieldset class="last">
		<div>
			<span class="spacer">&nbsp;</span>
			<span class="btn-container"><input type="submit" value="Register Your Interest" class="btn submitbtn" /></span>
		</div>
	</fieldset>
	
</form>