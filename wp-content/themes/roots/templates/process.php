<?php
if( isset($_POST) ){
	
	//form validation vars
	$formok = true;
	$errors = array();
	
	//sumbission data
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	$date = date('d/m/Y');
	$time = date('H:i:s');
	
	//form data
	$name 			= $_POST['cust-fullname'];	
	$email 			= $_POST['cust-email'];
	$telephone 		= $_POST['cust-contact'];
	$postcode		= $_POST['cust-pcode'];
	$address		= $_POST['cust-addrress'];
	
	$custinterests	= $_POST['cust-interests'];
	$custcomments	= $_POST['custcomments'];
	
	//checkes if select option is selected.	
	$radopt1 		= ($_POST['cm-ol-jdvn']) ? 'selected':'' ; 
	
	//$location		= $_POST['location'];

	//validate form data
	
	//validate name is not empty
	if(empty($name)){
		$formok = false;
		$errors[] = "You have not entered a name";
	}
	
	//validate email address is not empty
	if(empty($email)){
		$formok = false;
		$errors[] = "You have not entered an email address";
	//validate email address is valid
	}elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$formok = false;
		$errors[] = "You have not entered a valid email address";
	}
	
	//validate message is not empty
	/*if(empty($message)){
		$formok = false;
		$errors[] = "You have not entered a message";
	}
	//validate message is greater than 20 charcters
	elseif(strlen($message) < 20){
		$formok = false;
		$errors[] = "Your message must be greater than 20 characters";
	}
	*/
	//send email if all is ok
	if($formok){
		
		include_once 'includes/PHPMailer_5.2.1/class.phpmailer.php';
	
		$mail             = new PHPMailer(); // defaults to using php "mail()"
		
		$body             = file_get_contents('../emails/thankyou/index.html');
		$body             = eregi_replace("[\]",'',$body);
		
		$mail->SetFrom('info@thechampionssocceracademy.com', 'The Champions Soccer Academy - Frankie Burrows');
		
		$mail->AddReplyTo("info@thechampionssocceracademy.com","The Champions Soccer Academy - Frankie Burrows");
		
		$address = $email;
		$mail->AddAddress($address, $name);
		
		//$mail->AddAddress("", "");
		//$mail->AddAddress( "", "");
		$mail->AddBCC("awgthomas@ntlworld.com", "Andrew Thomas");
		$mail->AddBCC("simjlee22@aol.com", "Simon Lee");
		
		$mail->Subject    = "The Champions Soccer Academy - Web enquiry";
		
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		
		$mail->MsgHTML($body);
		
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		  
		}
		
		$mailTCSA = new PHPMailer();
		
		$mailTCSA->SetFrom('no-repy@thechampionssocceracademy.com', 'Website');
		
		
		$address = "f_burrows@hotmail.com";
		$mailTCSA->AddAddress($address, "Frankie Burrows");	
		$mailTCSA->AddBCC("awgthomas@ntlworld.com", "Andrew Thomas");
		$mailTCSA->AddBCC("simjlee22@aol.com", "Simon Lee");

		$mailTCSA->Subject    = "Web enquiry";
		
		$body = "<p>You have recieved a new message from the contact us form on your website.</p>
					  <p><strong>Name: </strong> {$name} </p>
					  <p><strong>Email Address: </strong> {$email} </p>
					  <p><strong>Telephone: </strong> {$telephone} </p>
					  
					  <p><strong>Address : </strong> {$address} </p>
					  <p><strong>Post Code : </strong> {$postcode} </p>
					
					  <p><strong>Main interest : </strong> {$custinterests} </p>
					  <p><strong>Comments : </strong> {$custcomments} </p>
					  <p><strong>Wishes to receive newsletters and updates : </strong> {$radopt1} </p>
					
					  
					  <p>This message was sent from the IP Address: {$ipaddress} on {$date} at {$time}</p>";
		
		$mailTCSA->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		
		$mailTCSA->MsgHTML($body);
		
		if(!$mailTCSA->Send()) {
		  echo "Mailer Error: " . $mailTCSA->ErrorInfo;
		} else {
		  
		}
		
	}
	
	//what we need to return back to our form
	$returndata = array(
		'posted_form_data' => array(
			'name' => $name,
			'email' => $email,
			'telephone' => $telephone,
			'enquiry' => $custcomments,
			'interests' => $custinterests
		),
		'form_ok' => $formok,
		'errors' => $errors
	);
		
	
	//if this is not an ajax request
	if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest'){
		//set session variables
		session_start();
		$_SESSION['cf_returndata'] = $returndata;
		
		//redirect back to form
		header('location: ' . $_SERVER['HTTP_REFERER']);
	}
}
