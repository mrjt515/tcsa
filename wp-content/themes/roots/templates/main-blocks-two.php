<section id="tcsa-pageblock2" class="w960 col3 clearfix">

	<article id="coachingstandards" class="block item1">
		<div class="thumbwindow"><img src="/assets/img/coaching-standards.jpg" width="280" height="173" alt="Our Coaching Standards" /></div>
		<h3>Our Coaching Standards</h3>
		<p>Our coaching curriculum goes way beyond the mandatory standards expected of junior and youth coaches. We set the highest standards of technical, tactical, physical, mental and training.</p>
		<?php // <a href="#" class="btn btn-style2" title="Read More about Our Coaching Standards">Read More</a> ?>
	</article>
	
	<article id="trainingfacilities" class="block item2">
		<div class="thumbwindow"><img src="/assets/img/coaching-facilities.jpg" width="280" height="173" alt="Coaching &amp; Training Facilities" /></div>
		<h3>Coaching &amp; Training Facilities</h3>
		<p>We coach and train locally in the Swansea area near the Liberty Stadium.</p>
		<?php // <a href="#" class="btn btn-style2" title="Read More about our Coaching &amp; Training Facilities">Read More</a> ?>
	</article>
	
	<article id="NLPtechniques" class="block item3">
		<div class="thumbwindow"><img src="/assets/img/npl-coaching-techniques.jpg" width="280" height="173" alt="NPL Coaching Techniques" /></div>
		<h3>NLP Coaching Techniques</h3>
		<p>Breaking new ground with NLP and other powerful tools in all of our coaching programmes.</p>
		<?php // <a href="#" class="btn btn-style2" title="Read More about NPL Coaching Techniques">Read More</a> ?>
	</article>
		
</section>