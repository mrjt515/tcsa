<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title">
    	<!--This is where the blog title is being set-->
    	<a href="<?php the_permalink(); ?>">
    	<?php the_title(); ?></a></h2>
    	<!--In that section there, just above-->
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
  	<!--Just before where the 'Continued' link is being created-->
    <?php the_excerpt(); ?>
  </div>
</article>
<!--A horizontal rule to show a distinct gap between the blogs-->
<hr/>