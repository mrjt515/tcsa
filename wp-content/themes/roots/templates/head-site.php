<!doctype html>
<?php get_template_part('templates/config','site'); ?>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><? echo $pageData['TITLE'].' | '.$compname; ?></title>
  <meta name="description" content="<? echo $pageData['DESC']; ?>">
  <meta name="keywords" content="<? echo $pageData['KEY']; ?>">
  <meta name="author" content="<? echo $designer; ?>">

  <link rel="stylesheet" href="code/css/global.css">
  <link rel="stylesheet" href="code/css/core-ui-select.css" media="screen">

  <script src="../assets/js/libs/modernizr-2.5.3.min.js"></script> 
  
  <!-- Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  
  <?php get_template_part('templates/utils', 'analytics'); ?>
   
</head>