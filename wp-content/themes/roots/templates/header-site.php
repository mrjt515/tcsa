<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico?5645645" />
<div id="headstrip" class="section-container">

	<header id="pagetop">
		
		<div class="w960 clearfix">		  		
			
			<hgroup class="left">
				<h1 id="sitelogo"><a href="/" title="Home">The Champion's Soccer Academy</a></h1>
				<h2 class="strapline hex1"><? echo $strapline; ?></h2>
				<h3 class="sponsor-plate"><span class="sponsor-ident">Sponsored By <? echo $sponsor_name; ?></span></h3>
			</hgroup>
			
			<hr />
				  
			<nav id="pagetop-menu" class="left">						
				<ul class="menu-container mainmenu clearfix">
					<?php
			          if (has_nav_menu('primary_navigation')) :
			            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav'));
			          endif;
			        ?>
				</ul>		 
			</nav>
				
		</div>
	
	</header>
	
</div>