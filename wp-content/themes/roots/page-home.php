<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Programmes' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post = $post_obj->post_content; 
?>	 
		<hr> 
		  
		<div id="mainsection" role="main">
		
			<div class="section-row-container pagetitle-head"></div>
		  
			<!-- Home Section Blocks	--> 
			<div class="section-row-container">
				<?php get_template_part('templates/main', 'blocks-home'); ?>
			</div>
			<!-- Home Section Blocks END -->		
			
			<hr>
			
			<!-- Main Content Section -->
			<div class="section-row-container row-highlight ltr">		
				<section id="tcsa-section1" class="w960">				
					<article class="boxcontainer promovid clearfix">
						<header class="boxcontent">
							<hgroup class="boxintro">
								<h2 class="boxheadline">Inspiring young players to excel and win in soccer and in life.</h2>
								<h3 class="boxblurb">Whether an aspiring pro or playing for the pure love of the game, our programmes are proven to help children become more confident, skilful, faster, stronger, healthier and happier.</h3>
							</hgroup>
							<p class="btn-container"><a href="/pre-qualify-registration/" title="Enquire about a placement for your child" class="btn btn-style1 cta1"><span>Find out if you qualify</span></a></p>
						</header>
						
						<figure class="boxpic bgfx">
							<iframe id="ytplayer" type="text/html" width="530" height="298.125"
							src="<?php echo $post; ?>"
							frameborder="0" allowfullscreen></iframe>
							<?php /*?><p class="btn-container"><a href="#" title="Share Video" class="btn">Share</a> <a href="#" title="Tweet Video" class="btn tweetbtn"><span>Tweet</span></a></p><?php */?>
						</figure>
					</article>				
				</section>			
			</div>
			
			<hr>
			
			<div class="section-row-container">			
				<section id="tcsa-section2" class="w960 ltr">				
					<article class="boxcontainer clearfix">
						
						<header class="boxcontent">
							<hgroup class="boxintro">
								<h2 class="boxheadline">Personal and professional development training.</h2>
								<h3 class="boxblurb">We provide a personalised development pathway for each individual from grass-roots right through to professional football with many of our players on the books of professional clubs.</h3>
							</hgroup>
							<p class="btn-container"><a href="/programmes/" title="Programmes" class="btn">View Our Courses</a> <a href="/e-book-download/" title="Download" class="btn btn-style1">Download our Free eBook</a></p>
						</header>
						
						<figure class="boxpic bgfx">
							<span class="imgstacked">
								<img src="assets/img/programmes/winning-attitude.jpg" width="340" height="210" alt="Welsh Super Cup 2012"/>
								<span class="imgstack l1"></span><span class="imgstack l2"></span>
							</span>
							<figcaption>TCSA Under 13's at the Welsh Super Cup 2012</figcaption>
						</figure>
											
					</article>				
				</section>			
			</div>
			
			<hr>		
		
		<div class="section-row-container last">
			<!-- Page Block Two -->						   
			<?php get_template_part('templates/main', 'blocks-one'); ?>												   
			<!-- Page Block Two END -->	
		</div>	


	</div><!-- Main Section END -->
		
	<hr>					   


		<!--</div>
	</div>
</div><!--WRAPPER end-->

	  	



<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts.php'); ?>
  
