
	<?php /*
	The method below finds the current pages ID and then sets the title (alongside the 'Register your Interest'
	button) so should the name of the page be changed from 'Fixtures & Results' then the title at the top will also change.
*/  
	$post_obj = $wp_query->get_queried_object();
	$post_ID = $post_obj->ID; 
?>



<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title"><? echo get_the_title($post_ID); ?></h2>
				<?php get_template_part('templates/cta', 'btn'); ?>	
				<?php get_template_part('templates/social', 'links'); ?>		
			</section>	
		</div>


<?php 
	$fixturesPages = get_pages(array('child_of'=>327));

	for ($i=0; $i < count($fixturesPages); $i++) { 
		$link = $fixturesPages[$i]->post_name;

		$sec_f = 	'<div class="section-row-container row-highlight">
							<section id="tcsa-section2" class="w960">
								<article class="contentbox clearfix">
								%2$s
								%1$s
								</article>
							</section>
						</div>';

			$head_f = 	'<header class="a">
							<h3>%s</h3>
							%s
							<p><a href="/programmes/'.$link.'/" title="Learn More" class="btn">Learn More</a></p>
						</header>';

			$img_f = 	'<figure class="b">
							<span class="imgstacked">
								<img src="%s" alt="%s" width="%d" height="%d" />
								<span class="imgstack l1"></span><span class="imgstack l2"></span>
							</span>
						</figure>';

			if($i%2 !=0) {
			$sec_f = 	'<div class="section-row-container">
							<section id="tcsa-section2" class="w960">
								<article class="contentbox clearfix">
								%2$s
								%1$s
								</article>
							</section>
						</div>
						<hr>';
			
		}
		
		$img = $img_alt = $img_width = $image = $img_height  = 0;

		if (has_post_thumbnail($fixturesPages[$i]->ID)):
			$image = wp_get_attachment_image_src(get_post_thumbnail_id($fixturesPages[$i]->ID), 'medium');
			$imgAlt = $fixturesPages[$i]->post_title;
			$width = $image[1];
			$height = $image[2];
		else:
			$img_f = '';
		endif;

		$post_title = $fixturesPages[$i]->post_title;
		//$sub_title  = 'This is the sub-title';
		$description = $fixturesPages[$i]->post_content;
		$description = apply_filters( 'the_content', $description );
		$description = str_replace( ']]>', ']]&gt;', $description );

		$img=$image[0];
		$imgPath = $fixturesPages[$i]->post_name;
		$imgSource = '/assets/img/programmes/'.$imgPath.'.jpg';
		$imgAlt = $fixturesPages[$i]->post_title;
		$width = $image[1];
		$height = $image[2];

		$divClass = 'row-highlight';

		$head = sprintf($head_f, $post_title, $description);
		$img = sprintf($img_f, $img, $imgAlt, $width, $height);

		$sec = sprintf($sec_f, $head, $img, $divClass);

		printf($sec, $head, $img);
	}
?>