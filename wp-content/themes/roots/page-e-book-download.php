
	
	<hr> 
	  
	<div id="mainsection" role="main">
	
		<div class="section-row-container pagetitle-head">
			<section id="tcsa-section0" class="w960 clearfix">				
				<h2 class="page-title">FREE Downloads</h2>
				<?php get_template_part('templates/cta', 'btn'); ?>
				<?php get_template_part('templates/social', 'links'); ?>				
			</section>	
		</div>
		
		<!-- Main Content Section -->
		<div class="section-row-container">		
			<section id="tcsa-section1" class="w960 page-intro">				
				<article class="clearfix">									
					<div class="contentbox section-intro">
					</div>					
				</article>					
			</section>			
		</div>
		
		<hr>
		
		<div class="section-row-container">			
			<section id="tcsa-section2" class="w960 ltr">			
				<article id="prog1" class="contentbox clearfix">			
					<header class="sideA">
						<?php get_template_part('templates/form', 'register'); ?>
					</header>
					<figure class="sideB">
						<img src="/assets/img/ebook-consumerguide-lrg.png" width="380" height="500" alt="Consumer Awarness Guide eBook"/>
						<figcaption class="hide">FREE eBook</figcaption>
					</figure>													
				</article>							
			</section>						
		</div>
		
		<hr>		
		
		<div class="section-row-container">
			<section id="tcsa-section9" class="w960 cta-block centered">			
				<article class="contentbox clearfix">
					<h3>Interested in Joining Us?</h3>
					<p>If any of these programmes interest you and your child then why not book a trial session with us today.</p>			
					<p class="btn-container"><a href="/pre-qualify-registration" title="Book a trial session today" class="btn btn-style1 cta2"><span>Book a Session Today</span></a></p>
					</article>							
			</section>			
		</div>
		
		<div class="section-row-container watermark-row">
			<h5 class="strapline-logo"><? echo $strapline; ?></h5>
		</div>	
			
	</div><!-- Main Section END -->
		
	<hr>					   
  


<!-- JavaScript at the bottom for fast page loading -->
<?php get_template_part('templates/js', 'scripts'); ?>
  
</body>
</html>