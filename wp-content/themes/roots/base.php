<?php /*The file containing the blog content is called home.php
Explained further in home.php*/ ?>


<?php get_template_part('templates/head'); ?>
  <body <?php body_class('two-column-layout'); ?>>

    <!--[if lt IE 7]><div class="alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</div><![endif]-->

          <!--Header Section-->  
            <?php
            do_action('get_header');
            
              get_template_part('templates/header', 'site');
           
            ?>
          <!--Header Section End-->

          <!--Banner Section-->
              <?php
              if(is_page('Home')) {
                get_template_part('banner');
              }
              else {
                get_template_part('templates/banner', 'site');
              }
              ?> 
          <!--Banner Section End-->
    
          <!--Main Section-->
          
            <div class="WRAPPER">
       
              <?php include roots_template_path(); ?>
           
            </div>
          <!--Main Section End-->
          
          <!--Footer Section-->
            <?php get_template_part('templates/footer', 'site'); ?>
          <!--Footer Section End-->
 

    <script type="text/javascript" src="assets/js/libs/jquery.easing.js"></script>
    <script type="text/javascript" src="assets/js/libs/jquery.h5validate.js"></script>
    <script src="assets/js/plugins-TCSA.js"></script>
    <script src="assets/js/script.js"></script>

  </body>

</html>

